VERSION = 0.07
ASM = nasm
CC = /opt/cross/bin/i686-elf-gcc
CFLAGS = -ffreestanding -std=c99  -Wall -Wextra -O0
LFLAGS = -ffreestanding -nostdlib -O0
INC = -I/opt/cross/lib/gcc/i686-elf/6.2.1/include
LIB = -L/opt/cross/lib/gcc/i686-elf/6.2.1

SRCA = sys/core/boot/prekernel.asm 
SRCA += sys/mem/mem_util.asm
SRCA += util/misc.asm
SRCA += sys/core/idt.asm sys/core/locks.asm

SRCC = kernel_start.c 
SRCC += util/conversion.c util/disasm.c util/string.c 
SRCC += sys/bin/elf/elf.c sys/bin/elf/kelf.c
SRCC += sys/core/cpu.c sys/core/idt.c
SRCC += sys/core/boot/multiboot.c
SRCC += sys/dev/ACPI.c sys/dev/ps2.c
SRCC += sys/dev/block/ATA.c sys/dev/block/ATAPI.c sys/dev/block/drive.c
SRCC += sys/dev/block/FAT/fat32.c
SRCC += sys/dev/pci/pci.c
SRCC += sys/dev/graphics/textmode/textmode_gfx.c
SRCC += sys/dev/input/keyboard.c
SRCC += sys/mem/mem_section.c sys/mem/vmm.c sys/mem/mem.c

OBJA = $(SRCA:%.asm=obj/%.oa)
OBJC = $(SRCC:%.c=obj/%.oc)
O_SRC = $(SRCC:%.c=src_out/%.asm)
OBJ = $(OBJA) $(OBJC)
OBJ_O = obj/kernel.obj
OUT = boot.iso

ifndef COMMENT
	COMMENT = Updated
endif

all: $(OUT)
	@echo Built $(OUT)
$(OUT): $(OBJ_O)
	@echo Building Image...
	@cp $(OBJ_O) _iso/boot/
	@echo '************************************************************************'
	@grub-mkrescue --product-version="$(VERSION)" --product-name="LeaOS" -o $(OUT) _iso
	@echo '************************************************************************'
	@echo Done
	@chmod 777 $(OUT)
$(OBJA) : $(SRCA:%.asm=src/%.asm)
	@echo Assembling Kernel Code: $(@F:.oa=.asm)
	@$(ASM) -felf32 $(patsubst obj/%, src/%, $(@:%.oa=%.asm)) -o $(@F:%.oa=obj/%.oa)
$(OBJC) : $(SRCC:%.c=src/%.c)
	@echo Compiling Kernel Code: $(@F:.oc=.c) 
	@$(CC) -c $(CFLAGS) $(INC) $(LIB) -o $(@F:%.oc=obj/%.oc) $(patsubst obj/%, src/%, $(@:%.oc=%.c))
$(OBJ_O) : $(OBJC) $(OBJA)
	@rm -f $(OUT)
	@sleep 1
	@echo Linking Kernel... 
	@$(CC) -T linker.ld -o $(OBJ_O) $(INC) $(LIB) $(LFLAGS) $(patsubst %, obj/%, $(^F)) -lgcc
source: $(O_SRC)
	@echo Built Mixed C Source
$(O_SRC): $(SRCC:%.c=src/%.c)
	@echo Compiling Kernel Code: $(@F:.s=.c) 
#@$(CC) -S -fverbose-asm $(CFLAGS) $(INC) $(LIB) -o src_out/$(@F:%.oc=%.s) $(patsubst src_out/%, src/%, $(@:%.s=%.c))
	@$(CC) -c -g -Wa,-a,-ad -masm=intel $(CFLAGS) $(INC) $(LIB) $(patsubst src_out/%, src/%, $(@:%.asm=%.c)) > src_out/$(@F:%.oc=%.asm)
	@rm *.o
clean:
	@echo Cleaning Files...
	@rm -rf obj/*.* $(OUT)
	@rm -f _iso/boot/*.*
	@rm -rf src_out/*.*
run:
	qemu-system-i386 -cpu core2duo,+sse4.1,+sse4.2 -m 512 -cdrom $(OUT) -boot d -drive file=hdd.img,if=ide,index=1,format=raw -curses
push:
	git add .
	git commit -a -m "$(COMMENT)"
	git push -u origin dev
merge:
	git checkout master
	git merge dev
	git push -u origin master
	git checkout dev
