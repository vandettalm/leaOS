#ifndef _STRING_H
#define _STRING_H

#include<stdint.h>
#include<stdbool.h>
#include<stdarg.h>

#include"conversion.h"

bool strcmp(const char *str1, const char *str2);
bool strncmp(const char *str1, const char *str2, uint32_t l);

uint32_t sputs(char *dst, const char *src);
void sprintf(char *dst, const char *fmt, ...);
void vsprintf(char *dst, const char *fmt, va_list args);

/*
*   *print* format
*   %A(B)
*
*   A:
*       i: integer
*       h: hex
*       b: bool
*       n: binary
*       c: char
*       s: string
*       !: color
*
*
*   B:
*       b: byte - 1
*       w: word - 2
*       d: dword - 4
*       q: qword - 8
*/

#endif