section .text

; 1b-init, 4b-a, 4b-b, 4b-c, 4b-d

global _getcpuid:function (_getcpuid.end - _getcpuid)

_getcpuid:
    push ebp
    mov ebp, esp

    push edi
    push ebx

    mov eax, [ebp+8]
    cpuid

    mov edi, [ebp+12]
    mov [edi], eax

    mov edi, [ebp+16]
    mov [edi], ebx

    mov edi, [ebp+20]
    mov [edi], ecx

    mov edi, [ebp+24]
    mov [edi], edx

    pop ebx
    pop edi

    leave
    ret
    .end:
