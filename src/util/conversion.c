#include "conversion.h"

static const char *hex_map = "0123456789ABCDEF";

void byte_to_hex(uint8_t b, char* hex_out) {
    hex_out[0] = hex_map[(b>>4)];
    hex_out[1] = hex_map[(b&0xF)];
    hex_out[2] = 0;
}

void word_to_hex(uint16_t w, char* hex_out) {
    hex_out[0] = hex_map[(w>>12) & 0xF];
    hex_out[1] = hex_map[(w>>8) & 0xF];
    hex_out[2] = hex_map[(w>>4) & 0xF];
    hex_out[3] = hex_map[w & 0xF];
    hex_out[4] = 0;
}

void dword_to_hex(uint32_t d, char* hex_out) {
    hex_out[0] = hex_map[(d>>28) & 0xF];
    hex_out[1] = hex_map[(d>>24) & 0xF];
    hex_out[2] = hex_map[(d>>20) & 0xF];
    hex_out[3] = hex_map[(d>>16) & 0xF];
    hex_out[4] = hex_map[(d>>12) & 0xF];
    hex_out[5] = hex_map[(d>>8) & 0xF];
    hex_out[6] = hex_map[(d>>4) & 0xF];
    hex_out[7] = hex_map[d & 0xF];
    hex_out[8] = 0;
}

void qword_to_hex(uint64_t q, char* hex_out) {
    hex_out[0] = hex_map[(q>>60) & 0xF];
    hex_out[1] = hex_map[(q>>56) & 0xF];
    hex_out[2] = hex_map[(q>>52) & 0xF];
    hex_out[3] = hex_map[(q>>48) & 0xF];
    hex_out[4] = hex_map[(q>>44) & 0xF];
    hex_out[5] = hex_map[(q>>40) & 0xF];
    hex_out[6] = hex_map[(q>>36) & 0xF];
    hex_out[7] = hex_map[(q>>32) & 0xF];
    hex_out[8] = hex_map[(q>>28) & 0xF];
    hex_out[9] = hex_map[(q>>24) & 0xF];
    hex_out[10] = hex_map[(q>>20) & 0xF];
    hex_out[11] = hex_map[(q>>16) & 0xF];
    hex_out[12] = hex_map[(q>>12) & 0xF];
    hex_out[13] = hex_map[(q>>8) & 0xF];
    hex_out[14] = hex_map[(q>>4) & 0xF];
    hex_out[15] = hex_map[q & 0xF];
    hex_out[16] = 0;
}

void byte_to_bin(uint8_t b, char* bin_out) {
    bin_out[8] = 0;
    for(uint8_t i = 0; i < 8; i++) {
        bin_out[7-i] = hex_map[(b>>i)&0x1];
    }
}

void word_to_bin(uint16_t b, char* bin_out) {
    bin_out[16] = 0;
    for(uint8_t i = 0; i < 16; i++) {
        bin_out[15-i] = hex_map[(b>>i)&0x1];
    }
}

void dword_to_bin(uint32_t b, char* bin_out) {
    bin_out[32] = 0;
    for(uint8_t i = 0; i < 32; i++) {
        bin_out[31-i] = hex_map[(b>>i)&0x1];
    }
}

void qword_to_bin(uint64_t b, char* bin_out) {
    bin_out[64] = 0;
    for(uint8_t i = 0; i < 64; i++) {
        bin_out[63-i] = hex_map[(b>>i)&0x1];
    }
}

void to_decimal(uint64_t d, char* bin_out) {
    uint64_t mul_count = 0, mul = 1;
    uint8_t count = 0, str_pos = 0;

    if(d == 0) {
        bin_out[0] = '0';
        bin_out[1] = 0;
    } else {
        while(d >= mul) {
            mul_count++;
            mul *= 10;
        }

        bin_out[mul_count] = 0;
        mul /= 10;

        for(mul_count = mul_count; mul_count != 0; mul_count--) {
            count = 0;
            while(d >= mul) {
                d -= mul;
                count++;
            }
            bin_out[str_pos] = hex_map[count];
            str_pos++;
            mul /= 10;
        }
    }
}

uint16_t byte_to_octal(uint8_t b) {
    return (b&0x7) | ((b&0x38)<<1) | ((b&0xC0)<<2);
}