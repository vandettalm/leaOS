#include "disasm.h"

#define ENCODE(t,a,b) ((((uint8_t)t&0x3) << 6) | (((uint8_t)a&0x7) << 3) | ((uint8_t)b&0x7))
#define TYPE(b) (b>>6)
#define ARG1(b) ((b>>3)&0x7)
#define ARG2(b) (b&0x7)

static asm_instruction decode;
static uint8_t state = 0;

//static const char *instruction_prefixes[] = {"LOCK", "REP", "REPE", "REPNE", "ES", "CS", "SS", "DS", "FS", "GS"};

static const char *registers_32bit[] = {"EAX", "ECX", "EDX", "EBX", "ESP", "EBP", "ESI", "EDI"};
static const char *registers_16bit[] = {"AX", "CX", "DX", "BX", "SP", "BP", "SI", "DI"};
static const char *registers_8bit[] = {"AL", "CL", "DL", "BL", "AH", "CH", "DH", "BH"};
//-----0x00 -> 0x3F
static const char *basic_instruction_group_1a[] = {"ADD", "OR", "ADC", "SBB", "AND", "SUB", "XOR", "CMP"};
static const char *basic_instruction_group_1b[] = {"PUSH ES", "PUSH SS", "PUSH DS", "PUSH CS", "POP ES", "POP SS", "POP DS", "DAA", "AAA", "DAS", "AAS"};

//-----0x40 -> 0x63
static const char *basic_instruction_group_2[] = {"INC", "DEC", "PUSH", "POP", "PUSHAD", "POPAD", "BOUND", "ARPL"};

//-----0x68 -> 0x6F
static const char *basic_instruction_group_3[] = {"IMUL", "INSB", "INSW", "INSD", "OUTSB", "OUTSW", "OUTSD"};

//-----0x70 -> 0x7F
static const char *basic_instruction_group_4[] = {"JO", "JNO", "JNAE", "JAE", "JZ", "JNZ", "JBE", "JNBE", "JS", "JNS", "JPE", "JPO", "JL", "JGE", "JLE", "JG"};

//-----0x80 -> 0x83 -> extended
//-----0x84 -> 0x97
static const char *basic_instruction_group_5[] = {"TEST", "XCHG", "MOV", "LEA", "NOP"};

//-----0x98 -> 0x9F
static const char *basic_instruction_group_6[] = {"CWDE", "CDQ", "CALLF", "FWAIT", "PUSHFD", "POPFD", "SAHF", "LAHF"};

//-----0xA0 -> 0xAF
static const char *basic_instruction_group_7[] = {"MOVSB", "MOVSW", "MOVSD", "CMPSB", "CMPSW", "CMPSD", "TEST", "STOSB", "STOSW", "STOSD", "LODSB", "LODSW", "LODSD", "SCASB", "SCASW", "SCASD"};

//-----0xB0 -> 0xBF -> extended MOV
//-----0xC0 -> 0xC1 -> shift group
static const char *basic_instruction_group_9[] = {"ROL", "ROR", "RCL", "RCR", "SHL", "SHR", "SAR"};

//-----0xC2 -> 0xCF
static const char *basic_instruction_group_10[] = {"RETN", "LES", "LDS", "ENTER", "LEAVE", "RETF", "INT", "INTO", "IRET", "IRETD"};

//-----0xD0 -> 0xD3 -> shift group2

void disasm_print() {
    printf("Prefixes: 0x%hb 0x%hb 0x%hb 0x%hb\n", decode.prefix_g1, decode.prefix_g2, decode.prefix_g3, decode.prefix_g4);
    printf("Opcodes: 0x%hb 0x%hb\n", decode.opcode_1, decode.opcode_2);
    printf("MOD REG RM: 0x%hb\n", decode.AMOS);
    printf("SIB: 0x%hb\n", decode.SIB);
    printf("Disp32: 0x%hd\n", decode.disp);
    printf("Imm32: 0x%hd\n", decode.im);
}

static int8_t disasm_get_basic_opcode_group() {
    uint8_t op1 = decode.opcode_1;
    if(op1 <= 0x3F) {
        return 1;
    } else if(op1 <= 0x63) {
        return 2;
    } else if(op1 <= 0x6F) {
        return 3;
    } else if(op1 <= 0x7F) {
        return 4;
    } else if(op1 <= 0x83) {
        return 0;
    } else if(op1 <= 0x97) {
        return 5;
    } else if(op1 <= 0x9F) {
        return 6;
    } else if(op1 <= 0xAF) {
        return 7;
    } else if(op1 <= 0xBF) {
        return 8;
    } else if(op1 <= 0xC1) {
        return 9;
    } else if(op1 <= 0xCF) {
        return 10;
    } else if(op1 <= 0xD3) {
        return 11;
    } else {
        return -1;
    }
}

static uint8_t disasm_prefix_type(uint8_t b) {
    if( b == 0xF0 || b == 0xF2 || b == 0xF3) {
        return 1;
    } else if( b == 0x2E || b == 0x36 || b == 0x3E || b == 0x26 || b == 0x64 || b == 0x65) {
        return 2;
    } else if( b == 0x66) {
        return 3;
    } else if( b == 0x67) {
        return 4;
    } else {
        return 0;
    }
}

static bool disasm_instruction_has_modrm() {
    uint8_t op1 = decode.opcode_1;
    uint8_t op2 = decode.opcode_2;
    int8_t g;

    if(op1 != 0x0F) {
        g = disasm_get_basic_opcode_group();
        switch(g) {
            case 0:
                return true;
            break;

            case 1:
                return ((op1 & 0xF) < 0x4) || (((op1 & 0xF) < 0xC) && ((op1 & 0xF) > 0x7));
            break;

            case 2:
                return ((op1 == 0x63) || (op1 == 0x64));
            break;

            case 3:
                return ((op1 != 0x68) && (op1 != 0x6A));
            break;

            case 4:
                return false;
            break;

            case 5:
                return (op1 < 0x90);
            break;

            case 6:
                return false;
            break;

            case 7:
                return false;
            break;

            case 8:
                return false;
            break;

            case 9:
                return true;
            break;

            case 10:
                return ((op1 > 0xC3) && (op1 < 0xC8));
            break;

            case 11:
                return true;
            break;

            default:
                return false;
            break;
        }
    } else {
        switch(op2) {
            default:
                return false;
            break;
        }
    }

    return false;
}

static uint8_t disasm_decode_instruction_prefix() {
    uint8_t op1 = decode.opcode_1;
    uint8_t out = 0, tmp;

    for(uint8_t* i = (void*)&decode; i < ((uint8_t*)(&decode)+4); i++) {
        tmp = disasm_prefix_type(*i);

        if(tmp == 1) {
            if(*i == 0xF0) {
                out |= 0x80;
            } else {
                out &= ~(0x6F);
                if(*i == 0xF2) {
                    out |= 0x20;
                } else {
                    if(op1 == 0x6D || op1 == 0x6C || op1 == 0xA4 || op1 == 0xA5 || op1 == 0xAA || op1 == 0xAB) {
                        out |= 0x60;
                    } else {
                        out |= 0x40;
                    }
                }
            }
        } else if(tmp == 2) {
            out &= ~(0x1C);
            if(*i == 0x2E) {
                out |= 0x04;
            } else if(*i == 0x36) {
                out |= 0x08;
            } else if(*i == 0x3E) {
                out |= 0x0C;
            } else if(*i == 0x26) {
                out |= 0x10;
            } else if(*i == 0x64) {
                out |= 0x14;
            } else if(*i == 0x65) {
                out |= 0x18;
            }
        } else if(tmp == 3) {
            out |= 0x02;
        } else if(tmp == 4) {
            out |= 0x01;
        }
    }
    return out;
}

static uint8_t disasm_instruction_disp_size() {
    if(disasm_instruction_has_modrm()) {
        AMOSbyte B;
        disasm_decode_AMOS(decode.AMOS, &B);
        if((B.MOD == 0 && B.RM == 0x5) || B.MOD == 0x02) {
            if((disasm_decode_instruction_prefix() & 0x02) == 0)
                return 4;
            else
                return 2;
        } else if (B.MOD == 0x01 ) {
            return 1;
        } else {
            return 0;
        }
    } else {
        return 4;
    }
}

static uint8_t disasm_instruction_imm_size() {
    int8_t g = disasm_get_basic_opcode_group();
    AMOSbyte B;
    disasm_decode_AMOS(decode.AMOS, &B);
    uint8_t op1 = decode.opcode_1;
    uint8_t op2 = decode.opcode_2;
    uint8_t pfx = disasm_decode_instruction_prefix() & 0x2;

    if(op1 != 0x0F) {
        if(g == 0) {
            if(op1 == 0x80 || op1 == 0x83) {
                return 1;
            } else {
                if(pfx == 0) {
                    return 4;
                } else {
                    return 2;
                }
            }
        } else if(g == 1) {
            if((op1 & 0x7) == 0x4) {
                return 1;
            } else if((op1 & 0x7) == 0x5) {
                if(pfx == 0) {
                    return 4;
                } else {
                    return 2;
                }
            }
        } else if(g == 3) {
            if(op1 == 0x68 || op1 == 0x69) {
                if(pfx == 0) {
                    return 4;
                } else {
                    return 2;
                }
            } else {
                if (op1 == 0x6A || op1 == 0x6B) {
                    return 1;
                }
            }
        } else if(g == 7) {
            if(op1 == 0xA8) {
                return 1;
            } else {
                if(pfx == 0) {
                    return 4;
                } else {
                    return 2;
                }
            }
        } else if(g == 8) {
            if (op1 >= 0xB0 && op1 <= 0xB7) {
                return 1;
            } else {
                if(pfx == 0) {
                    return 4;
                } else {
                    return 2;
                }
            }
        } else if(g == 9 || g == 10) {
            if(op1 == 0xC0 || op1 == 0xC1 || op1 == 0xC6 || op1 == 0xCD) {
                return 1;
            } else if(op1 == 0xC2) {
                return 2;
            } else if(op1 == 0xC8) {
                return 3;
            } else if(op1 == 0xC7) {
                if(pfx == 0) {
                    return 4;
                } else {
                    return 2;
                }
            }
        } else if(g == 11) {
            if(op1 == 0xD0 || op1 == 0xD2) {
                return 1;
            } else if(op1 == 0xD1 || op1 == 0xD3) {
                if(pfx == 0) {
                    return 4;
                } else {
                    return 2;
                }
            }
        }
    } else {
        return op2 - op2;
    }
    return 0;
}

static bool disasm_instruction_has_sib() {
    AMOSbyte b;
    disasm_decode_AMOS(decode.AMOS, &b);
    return ((b.MOD != 0x3) && b.RM == 0x4);
}

static bool disasm_instruction_has_disp() {
    if(disasm_instruction_has_modrm()) {
        AMOSbyte B;
        disasm_decode_AMOS(decode.AMOS, &B);
        return ((B.MOD == 0 && B.RM == 0x5) || B.MOD == 0x01 || B.MOD == 0x02);
    } else {
        uint8_t op1 = decode.opcode_1;
        return ((op1 >= 0x70 && op1 <= 0x7F) || op1 == 0x9A || (op1 >= 0xA0 && op1 <= 0xA3));
    }
}

static bool disasm_instruction_has_imm() {
    uint8_t op1 = decode.opcode_1;
    uint8_t op2 = decode.opcode_2;
    int8_t g;

    if(op1 != 0x0F) {
        g = disasm_get_basic_opcode_group();
        switch(g) {
            case 0:
                return true;
            break;

            case 1:
                return !((op1 & 0xF) < 0x4) || (((op1 & 0xF) < 0xC) && ((op1 & 0xF) > 0x7));
            break;

            case 2:
                return false;
            break;

            case 3:
                return (op1 < 0x6C);
            break;

            case 4:
                return false;
            break;

            case 5:
                return (op1 < 0x84);
            break;

            case 6:
                return false;
            break;

            case 7:
                return ((op1 == 0xA8) || (op1 == 0xA9));
            break;

            case 8:
                return true;
            break;

            case 9:
                return true;
            break;

            case 10:
                return ((op1 == 0xC6) || (op1 == 0xC7) || (op1 == 0xC8) || (op1 == 0xCA) || (op1 == 0xCD));
            break;

            case 11:
                return false;
            break;

            default:
                return false;
            break;
        }
    } else {
        return (op2 == 1);
    }
}
/* UNUSED ATM
static bool disasm_instruction_has_static_register() {
    uint8_t op1 = decode.opcode_1;
    //uint8_t op2 = decode.opcode_2;

    if(op1 != 0x0F) {
        int8_t g = disasm_get_basic_opcode_group();
        if (g == 1) {
            if((op1 & 0x7) == 0x4) {
                return true;
            } else if((op1 & 0x7) == 0x5) {
                return true;
            }
        } else if (g == 2) {
            return (op1 <= 0x5F);
        } else if (g == 5) {
            return (op1 <= 0x97) && (op1 >= 0x90);
        } else if (g == 7) {
            if((op1&0x1) == 0) {
                return true;
            } else {
                return true;
            }
        } else if (g == 8) {
            return true;
        } else if (g == 11) {
            return true;
        }
        return false;
    } else {
        return false;//!(op2 == op2);
    }
}
*/
/* UNUSED ATM
static bool disasm_instruction_has_plain_imm() {
    uint8_t op1 = decode.opcode_1;
    uint8_t op2 = decode.opcode_2;
    int8_t g;

    if(op1 != 0x0F) {
        g = disasm_get_basic_opcode_group();
        switch(g) {
            case 1:
                return (((op1 & 0x7) == 0x4) || ((op1 & 0x7) > 0x5));
            break;

            case 3:
                return (op1 == 0x68) || (op1 == 0x6A);
            break;

            case 7:
                return ((op1 == 0xA8) || (op1 == 0xA9));
            break;

            case 8:
                return true;
            break;

            case 10:
                return ((op1 == 0xC2) || (op1 == 0xCA) || (op1 == 0xCD));
            break;

            default:
                return false;
            break;
        }
    } else {
        return (op2 == 1);
    }
}
*/
static const char* disasm_decode_prefix() {
    return NULL;
}
/* UNUSED ATM
static const char* disasm_get_segment_override() {
    uint8_t t = 0;
    for(uint32_t i = 0; i < 4; i++) {
        if(disasm_prefix_type(((uint8_t*)&decode)[i]) == 2) {
            t = ((uint8_t*)&decode)[i];
        }
    }

    if(t == 0) {
        return NULL;
    } else {
        if(t == 0x26) {
            return instruction_prefixes[4];
        } else if(t == 0x2E) {
            return instruction_prefixes[5];
        } else if(t == 0x36) {
            return instruction_prefixes[6];
        } else if(t == 0x3E) {
            return instruction_prefixes[7];
        } else if(t == 0x64) {
            return instruction_prefixes[8];
        } else if(t == 0x65) {
            return instruction_prefixes[9];
        }
    }
}
*/
static const char* disasm_decode_register(uint8_t r, uint8_t t) {
    if(t == 0) {
        return registers_32bit[r];
    } else if(t == 1) {
        return registers_16bit[r];
    } else {
        return registers_8bit[r];
    }
}

static const char* disasm_decode_static_register() {
    uint8_t op1 = decode.opcode_1;
    //uint8_t op2 = decode.opcode_2;

    if(op1 != 0x0F) {
        int8_t g = disasm_get_basic_opcode_group();
        if (g == 1) {
            if((op1 & 0x7) == 0x4) {
                return registers_8bit[0];
            } else if((op1 & 0x7) == 0x5) {
                //add size checking
                return registers_32bit[0];
            }
        } else if (g == 2) {
            //add size checking
            return disasm_decode_register((op1&0x7), 0);
        } else if (g == 5) {
            //add size checking
            return disasm_decode_register((op1&0x7), 0);
        } else if (g == 7) {
            if((op1&0x1) == 0) {
                return registers_8bit[0];
            } else {
                //add size checking
                return registers_32bit[0];
            }
        } else if (g == 8) {
            if(op1 > 0xB7) {
                //add size checking
                return disasm_decode_register((op1&0x7), 0);
            } else {
                //add size checking
                return disasm_decode_register((op1&0x7), 2);
            }
        } else if (g == 11) {
            return registers_8bit[1];
        } else {
            return NULL;
        }
    } else {
        return NULL;//(const char*)&op2;
    }

    return NULL;
}

static const char* disasm_decode_basic_instruction_group1() {
    if((decode.opcode_1 & 0x7) < 6) {
        return basic_instruction_group_1a[byte_to_octal(decode.opcode_1)>>4];
    }
    else {
        switch(decode.opcode_1) {
            case 0x06:
                return basic_instruction_group_1b[0];
            case 0x07:
                return basic_instruction_group_1b[4];
            case 0x16:
                return basic_instruction_group_1b[1];
            case 0x17:
                return basic_instruction_group_1b[5];
            case 0x27:
                return basic_instruction_group_1b[7];
            case 0x37:
                return basic_instruction_group_1b[8];
            case 0x0e:
                return basic_instruction_group_1b[3];
            case 0x1e:
                return basic_instruction_group_1b[2];
            case 0x1f:
                return basic_instruction_group_1b[6];
            case 0x2f:
                return basic_instruction_group_1b[9];
            case 0x3f:
                return basic_instruction_group_1b[10];
            default:
                return "PREFIX";
        }
    }
}

static const char* disasm_decode_basic_instruction_group1_extension() {
    return basic_instruction_group_1a[(decode.AMOS & 0x38) >> 3];
}

static const char* disasm_decode_basic_instruction_group2() {
    uint8_t op1 = decode.opcode_1;
    if(op1 < 0x60) {
        return basic_instruction_group_2[(op1&0x18)>>3];
    } else {
        return basic_instruction_group_2[4 + (op1&0x03)];
    }
}

static const char* disasm_decode_basic_instruction_group3() {
    uint8_t op1 = decode.opcode_1;
    if(op1 == 0x68 || op1 == 0x6A) {
        return basic_instruction_group_2[2];
    } else if(op1 == 0x69 || op1 == 0x6B) {
        return basic_instruction_group_3[0];
    } else {
        op1 -= 0x6C;
        if(op1 == 0) {
            return basic_instruction_group_3[1];
        } else if(op1 == 1) {
            //need to add some prefix checking for size
            return basic_instruction_group_3[2];
        } else if(op1 == 2) {
            return basic_instruction_group_3[4];
        } else if(op1 == 3) {
            //need to add some prefix checking for size
            return basic_instruction_group_3[5];
        }
    }
    return NULL;
}

static const char* disasm_decode_basic_instruction_group4() {
    return basic_instruction_group_4[(decode.opcode_1 &0x0F)];
}

static const char* disasm_decode_basic_instruction_group5() {
    uint8_t op1 = decode.opcode_1;

    if(op1 == 0x84 || op1 == 0x85) {
        return basic_instruction_group_5[0];
    } else if(op1 == 0x86 || op1 == 0x87) {
        return basic_instruction_group_5[1];
    } else if(op1 <= 0x8C || op1 == 0x8E) {
        return basic_instruction_group_5[2];
    } else if(op1 == 0x8D) {
        return basic_instruction_group_5[3];
    } else if(op1 == 0x90) {
        return basic_instruction_group_5[4];
    } else {
        return basic_instruction_group_5[1];
    }
}

static const char* disasm_decode_basic_instruction_group6() {
    uint8_t op1 = decode.opcode_1 - 0x98 - ((decode.opcode_1 >= 0x9B)?1:0);
    return basic_instruction_group_6[op1];
}

static const char* disasm_decode_basic_instruction_group7() {
    uint8_t op1 = decode.opcode_1 - 0xA0;
    uint8_t pft = disasm_decode_instruction_prefix() & 0x3;

    if(op1 < 4) {
        return basic_instruction_group_5[2];
    } else if(op1 == 4) {
        return basic_instruction_group_7[0];
    } else if(op1 == 5) {
        if((pft & 0x2) == 0)
            return basic_instruction_group_7[2];
        else
            return basic_instruction_group_7[1];
    } else if(op1 == 6) {
        return basic_instruction_group_7[3];
    } else if(op1 == 7) {
        if((pft & 0x2) == 0)
            return basic_instruction_group_7[5];
        else
            return basic_instruction_group_7[4];
    } else if(op1 == 8 || op1 == 9) {
        return basic_instruction_group_7[6];
    } else if(op1 == 10) {
        return basic_instruction_group_7[7];
    } else if(op1 == 11) {
        if((pft & 0x2) == 0)
            return basic_instruction_group_7[9];
        else
            return basic_instruction_group_7[8];
    } else if(op1 == 12) {
        return basic_instruction_group_7[10];
    } else if(op1 == 13) {
        if((pft & 0x2) == 0)
            return basic_instruction_group_7[12];
        else
            return basic_instruction_group_7[11];
    } else if(op1 == 14) {
        return basic_instruction_group_7[13];
    } else if(op1 == 15) {
        if((pft & 0x2) == 0)
            return basic_instruction_group_7[15];
        else    
            return basic_instruction_group_7[14];
    }

    return NULL;
}

static const char* disasm_decode_basic_instruction_group8() {
    return basic_instruction_group_5[2];
}

static const char* disasm_decode_basic_instruction_group9() {
    uint8_t loc = (decode.AMOS & 0x38) >> 3;
    if(loc < 6) {
        return basic_instruction_group_9[loc];
    } else {
        if(loc == 6) {
            return basic_instruction_group_9[4];
        } else {
            return basic_instruction_group_9[6];
        }
    }
}

static const char* disasm_decode_basic_instruction_group10() {
    uint8_t op1 = decode.opcode_1;

    if(op1 == 0xC2 || op1 == 0xC3) {
        return basic_instruction_group_10[0];
    } else if(op1 == 0xC4) {
        return basic_instruction_group_10[1];
    } else if(op1 == 0xC5) {
        return basic_instruction_group_10[2];
    } else if(op1 == 0xC6 || op1 == 0xC7) {
        return basic_instruction_group_5[2];
    } else if(op1 == 0xC8) {
        return basic_instruction_group_10[3];
    } else if(op1 == 0xC9) {
        return basic_instruction_group_10[4];
    } else if(op1 == 0xCB || op1 == 0xCA) {
        return basic_instruction_group_10[5];
    } else if(op1 == 0xCD || op1 == 0xCC) {
        return basic_instruction_group_10[6];
    } else if(op1 == 0xCE) {
        return basic_instruction_group_10[7];
    } else if(op1 == 0xCF) {
        //need to add some prefix checking for size
        return basic_instruction_group_10[9];
    }

    return NULL;
}

static const char* disasm_decode_basic_instruction_group11() {
    return disasm_decode_basic_instruction_group9();
}

static const char *disasm_decode_basic_instruction() {
    switch (disasm_get_basic_opcode_group()) {
        case 0:
            return disasm_decode_basic_instruction_group1_extension();
        break ;

        case 1:
            return disasm_decode_basic_instruction_group1();
        break;

        case 2:
            return disasm_decode_basic_instruction_group2();
        break;

        case 3:
            return disasm_decode_basic_instruction_group3();
        break;

        case 4:
            return disasm_decode_basic_instruction_group4();
        break;

        case 5:
            return disasm_decode_basic_instruction_group5();
        break;

        case 6:
            return disasm_decode_basic_instruction_group6();
        break;
        
        case 7:
            return disasm_decode_basic_instruction_group7();
        break;

        case 8:
            return disasm_decode_basic_instruction_group8();
        break;

        case 9:
            return disasm_decode_basic_instruction_group9();
        break;

        case 10:
            return disasm_decode_basic_instruction_group10();
        break;

        case 11:
            return disasm_decode_basic_instruction_group11();
        break;

        default:
            return "ERROR";
        break;
    }
}

static bool disasm_state_machine(uint8_t b) {
    uint8_t tmp = disasm_prefix_type(b);
    //printf("State: %ib\n", state);
    if(state < 4) {
        if(tmp == 0) {
            decode.pfx = disasm_decode_instruction_prefix();
            decode.opcode_1 = b;
            if(b == 0x0F)
                state = 5;
            else
                state = 6;
            return false;
        } else {
            ((uint8_t*)(&decode))[state] = b;
            state++;
            return false;
        }
    } else if(state < 6) {
        if(tmp != 0) {
            disasm_reset_state();
            return false;
        } else {
            ((uint8_t*)(&decode))[state] = b;
            state++;
            return false;
        }
    } else if(state == 6) {
        if(disasm_instruction_has_modrm()) {
            decode.AMOS = b;
            if(disasm_instruction_has_sib()) {
                state = 7;
                return false;
            } else {
                if(disasm_instruction_has_disp()) {
                    state = 8;
                    return false;
                } else if(disasm_instruction_has_imm()) {
                    state = 12;
                    return false;
                } else {
                    state = 15;
                    return true;
                }
            }
        } else {
            if(disasm_instruction_has_disp()) {
                state = 8;
                ((uint8_t*)(&decode))[state] = b;
                state++;
                return false;
            } else if(disasm_instruction_has_imm()) {
                state = 12;
                ((uint8_t*)(&decode))[state] = b;
                state++;
                return false;
            } else {
                state = 16;
                return true;
            }
        }
    } else if(state == 7) {
        decode.SIB = b;
        if(disasm_instruction_has_disp()) {
            state = 8;
            return false;
        } else if(disasm_instruction_has_imm()) {
            state = 12;
            return false;
        } else {
            return true;
        }
    } else if(state >= 8 && state <= 11) {
        ((uint8_t*)(&decode))[state] = b;
        state++;
        if((state - 8) >= disasm_instruction_disp_size()) {
            if(disasm_instruction_has_imm()) {
                state = 12;
                return false;
            } else {
                state = 16;
                return true;
            }
        }
    } else if(state >= 12) {
        ((uint8_t*)(&decode))[state] = b;
        state++;
        if((state - 12) >= disasm_instruction_imm_size()) {
            state = 16;
            return true;
        }
    }

    return false;
}

static void disasm_encode_instruction_format() {
    uint8_t op1 = decode.opcode_1;
    //uint8_t op2 = decode.opcode_2;

    if(op1 != 0x0F) {

        if(op1 == 0xC2 || op1 == 0x68 || op1 == 0x6A || op1 == 0xCA || op1 == 0xCD) {
            decode.via = ENCODE(1,4,0); //I
        } else if((op1 >= 0x70 && op1 <= 0x7F) || op1 == 0x9A) {
            decode.via = ENCODE(1,5,0); //D
        } else if((op1 < 0x3F && (((op1&0x7) == 2) || ((op1&0x7) == 3))) || op1 == 0x62 || op1 == 0x8A || op1 == 0x8B || op1 == 0x8D) {
            decode.via = ENCODE(2,1,2); //RM
        } else if((op1 < 0x3F && (((op1&0x7) == 0) || ((op1&0x7) == 1))) || op1 == 0x63 || (op1 >= 0x84 && op1 <= 0x89) || op1 == 0x8C) {
            decode.via = ENCODE(2,2,1); //MR
        } else if(op1 == 0x80 || op1 == 0x81 || op1 == 0x82 || op1 == 0x83 || op1 == 0xC0 || op1 == 0xC1 || op1 == 0xC6 || op1 == 0xC7) {
            decode.via = ENCODE(2,2,4); //MI
        } else if((op1 < 0x3F && (((op1&0x7) == 4) || ((op1&0x7) == 5))) || (op1 >= 0xB0 && op1 <= 0xBF) || op1 == 0xA8 || op1 == 0xA9) {
            decode.via = ENCODE(2,3,4); //SI
        } else if(op1 == 0xA0 || op1 == 0xA1) {
            decode.via = ENCODE(2,3,5); //SD
        } else if(op1 == 0xA2 || op1 == 0xA3) {
            decode.via = ENCODE(2,5,3); //DS
        } else {
            decode.via = 0;
        }
    }
}

void disasm_reset_state() {
    state = 0;
    decode.prefix_g1 = 0x00;
    decode.prefix_g2 = 0x00;
    decode.prefix_g3 = 0x00;
    decode.prefix_g4 = 0x00;
    decode.opcode_1 = 0x00;
    decode.opcode_2 = 0x00;
    decode.AMOS = 0x00;
    decode.SIB = 0x00;
    decode.disp = 0x00000000;
    decode.im = 0x00000000;
    decode.pfx = 0x00;
    decode.via = 0x00;
}

void disasm_decode_AMOS(uint8_t b, AMOSbyte* out) {
    out->RM = b & 0x7;
    out->REG = (b & 0x38) >> 3;
    out->MOD = (b & 0xC0) >> 6;
}

void disasm_decode_AMOS_s(uint8_t b, AMOSbyte* out) {
    out->REG = b & 0x7;
    out->RM = b & 0x38;
    out->MOD = b & 0xC0;
}

static void disasm_build_memory_string(char *dst) {
    AMOSbyte modrm, sib;
    char tmpStr[64];

    disasm_decode_AMOS(decode.AMOS, &modrm);
    disasm_decode_AMOS(decode.SIB, &sib);

    if(modrm.MOD != 0x3) {
        dst[0] = '[';
        dst++;
    }

    uint8_t n = 1;
    for(uint8_t i = 1; i <= sib.MOD; i++)
    {
        n *= 2;
    }

    //printf("(%ib,%ib,%ib)\n", modrm.MOD, modrm.REG, modrm.RM);

    if(modrm.MOD == 0) {
        if(modrm.RM == 0x4) {
            to_decimal((uint64_t)n, tmpStr);
            dst += sputs(dst, disasm_decode_register(sib.REG, 0));
            dst += sputs(dst, "*");
            dst += sputs(dst, tmpStr);
            dst += sputs(dst, " + ");
            if(sib.RM == 0x5) {
                dword_to_hex(decode.disp, tmpStr);
                dst += sputs(dst, "0x");
                dst += sputs(dst, tmpStr);
            } else {
                dst += sputs(dst, disasm_decode_register(sib.RM, 0));
            }
        } else if(modrm.RM == 0x5) {
            dword_to_hex(decode.disp, tmpStr);
            dst += sputs(dst, "0x");
            dst += sputs(dst, tmpStr);
        } else {
            dst += sputs(dst, disasm_decode_register(modrm.RM, 0));
        }
    } else if(modrm.MOD != 0x3) {
        if(modrm.RM == 0x4) {
            to_decimal((uint64_t)n, tmpStr);
            dst += sputs(dst, disasm_decode_register(sib.REG, 0));
            dst += sputs(dst, "*");
            dst += sputs(dst, tmpStr);
            dst += sputs(dst, " + ");
            dst += sputs(dst, disasm_decode_register(sib.RM, 0));
            dst += sputs(dst, " + 0x");
            if(modrm.MOD == 0x1) {
                byte_to_hex((uint8_t)decode.disp, tmpStr);
                dst += sputs(dst, tmpStr);
            } else {
                dword_to_hex(decode.disp, tmpStr);
                dst += sputs(dst, tmpStr);
            }
        } else {
            dst += sputs(dst, disasm_decode_register(modrm.RM, 0));
            dst += sputs(dst, " + 0x");
            if(modrm.MOD == 0x1) {
                byte_to_hex((uint8_t)decode.disp, tmpStr);
                dst += sputs(dst, tmpStr);
            } else {
                dword_to_hex(decode.disp, tmpStr);
                dst += sputs(dst, tmpStr);
            }
        }
    } else {
        dst += sputs(dst, disasm_decode_register(modrm.RM, 0));
    }

    if(modrm.MOD != 0x3) {
        dst[0] = ']';
        dst[1] = 0;
    } else {
        dst[0] = 0;
    }
}

static char* disasm_get_arg(uint8_t t, char* dst) {
    AMOSbyte b;
    disasm_decode_AMOS(decode.AMOS, &b);

    if(t == 1) {
        //size checking
        dst += sputs(dst, disasm_decode_register(b.REG, 0));
    } else if(t == 2) {
        char tmp[64];
        disasm_build_memory_string(tmp);
        dst += sputs(dst, tmp);
    } else if(t == 3) {
        dst += sputs(dst, disasm_decode_static_register());
    } else if(t == 4) {
        char tmp[64];
        if(disasm_instruction_imm_size() == 4) {
            sprintf(tmp, "0x%hd", decode.im); 
        } else if(disasm_instruction_imm_size() == 2) {
            sprintf(tmp, "0x%hw", (uint16_t)decode.im);
        } else {
            sprintf(tmp, "0x%hb", (uint8_t)decode.im);
        }
        dst += sputs(dst, tmp);
    } else if(t == 5) {
        char tmp[64];
        const char *pfx = disasm_decode_prefix();

        sprintf(tmp, "%s:", (pfx == NULL)?"DS":pfx);
        dst += sputs(dst, tmp);

        if(disasm_instruction_disp_size() == 4) {
            sprintf(tmp, "0x%hd", decode.disp); 
        } else if(disasm_instruction_disp_size() == 2) {
            sprintf(tmp, "0x%hw", (uint16_t)decode.disp);
        } else {
            sprintf(tmp, "0x%hb", (uint8_t)decode.disp);
        }
        dst += sputs(dst, tmp);
    }

    return dst;
}

static void disasm_print_via() {
    uint8_t t = TYPE(decode.via);
    uint8_t a = ARG1(decode.via);
    uint8_t b = ARG2(decode.via);

    char out_string[256];
    char *tmp = out_string;

    tmp += sputs(tmp, "%*");
    tmp[0] = 0x0C;
    tmp++;
    tmp += sputs(tmp, disasm_decode_basic_instruction());

    if(t == 2) {
        tmp += sputs(tmp, " %*");
        tmp[0] = 0x0D;
        tmp++;
        tmp = disasm_get_arg(a, tmp);
        tmp += sputs(tmp, "%*");
        tmp[0] = 0x0A;
        tmp++;
        tmp += sputs(tmp, ", %*");
        tmp[0] = 0x0E;
        tmp++;
        tmp = disasm_get_arg(b, tmp);
    } else if(t == 1) {

    } else if(t == 0) {

    } else {

    }

    tmp[0] = 0;
    printf("(0x%hb) %s\n", decode.via, out_string);
}

void disasm(uint32_t addr, uint32_t len) {
    uint8_t *data = (void*)addr;

    for(uint32_t j = 0; j < len; j++) {
        //char tmpStr[64];
        disasm_reset_state();
        uint8_t i = 0;
        while(disasm_state_machine(data[0]) == false) {
            data++;
            i++;
        }
        data++;
        i++;

        disasm_encode_instruction_format();
        printf("(%ib %hd %hd)", i, decode.im, decode.disp);
        disasm_print_via();
    }

    //disasm_print();
    //disasm_build_memory_string(tmpStr);
    //printf("Instruction: %s\n", disasm_decode_basic_instruction());
    //printf("Has ModRM: %s\n", (disasm_instruction_has_modrm())?"True":"False");
    //printf("Has SIB: %s\n", (disasm_instruction_has_sib())?"True":"False");
    //printf("Has Disp: %s\n", (disasm_instruction_has_disp())?"True":"False");
    //printf("Has IMM: %s\n", (disasm_instruction_has_imm())?"True":"False");
    //printf("IMM Size: 0x%hb\n", disasm_instruction_imm_size());
    //printf("DISP Size: 0x%hb\n", disasm_instruction_disp_size());
    //printf("Memory Test: %s\n", tmpStr);
    //if(disasm_instruction_has_static_register()) {
    //    printf("Static Register: %s\n", disasm_decode_static_register());
    //}
}

#undef ENCODE
#undef TYPE
#undef ARG1
#undef ARG2