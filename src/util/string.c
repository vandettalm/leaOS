#include"string.h"

bool strcmp(const char *str1, const char *str2) {
    uint32_t i;

    for(i = 0; ((str1[i] != 0) && (str2[i] != 0)); i++) {
        if(str1[i] != str2[i])
            return false;
    }

    return ((str1[i] == 0) && (str2[i] == 0));
}

bool strncmp(const char *str1, const char *str2, uint32_t l) {
    for(uint32_t i = 0; i < l; i++) {
        if(str1[i] != str2[i]) {
            return false;
        }
    }

    return true;
}

uint32_t sputs(char *dst, const char *src) {
    uint32_t i = 0;

    for(i = 0; src[i] != 0; i++) {
        dst[i] = src[i];
    }

    return i;
}

void sprintf(char* dst, const char*fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vsprintf(dst, fmt, args);
    va_end(args);
}

void vsprintf(char* dst, const char*fmt, va_list args) {
    uint8_t state = 0;
    char c;
    char *tStr;
    bool hex = false;
    bool bin = false;
    uint32_t tmp;
    uint32_t i;

    BIN_STR(TO_STR);

    for(i = 0; fmt[i] != 0; i++) {
        c = fmt[i];
        if(state == 0) {
            if(c == '%') {
                state = 1;
            } else if(c == '\t') { 
                dst[0] = ' ';
                dst[1] = ' ';
                dst[2] = ' ';
                dst[3] = ' ';
                dst += 4;
            } else {
                dst[0] = c;
                dst++;
            }
        } else if(state == 1) {
            if(c == 'i') {
                state = 2;
                hex = false;
                bin = false;
            } else if(c == 'h') {
                state = 2;
                hex = true;
                bin = false;
            } else if(c == 'n') {
                state = 2;
                bin = true;
                hex = false;
            } else if(c == 'b') {
                state = 0;
                hex = (bool)va_arg(args, uint32_t);
                tmp = sputs(dst, (hex)?"True":"False");
                dst += tmp;
            } else if(c == 'c') {
                state = 0;
                c = (char)va_arg(args, uint32_t);
                dst[0] = c;
                dst++;
            } else if(c == 's') {
                state = 0;
                tStr = va_arg(args, char*);
                tmp = sputs(dst, tStr);
                dst += tmp;
            } else if(c == '!') {
                state = 0;
                tmp = sputs(dst, "%*");
                dst += tmp;
                dst[0] = (char)va_arg(args, uint32_t);
                dst++;
            } else {
                dst[0] = '%';
                dst++;
                dst[0] = c;
                dst++;
            }
        } else if(state == 2) {
            state = 0;
            if(c == 'b') {
                if(hex && !bin) {
                    byte_to_hex((uint8_t)va_arg(args, uint32_t), TO_STR);
                } else if(bin) {
                    byte_to_bin((uint8_t)va_arg(args, uint32_t), TO_STR);
                } else {
                    to_decimal((uint64_t)va_arg(args, uint32_t), TO_STR);
                }
            } else if(c == 'w') {
                if(hex && !bin) {
                    word_to_hex((uint16_t)va_arg(args, uint32_t), TO_STR);
                } else if(bin) {
                    word_to_bin((uint16_t)va_arg(args, uint32_t), TO_STR);
                } else {
                    to_decimal((uint64_t)va_arg(args, uint32_t), TO_STR);
                }
            } else if(c == 'd') {
                if(hex && !bin) {
                    dword_to_hex(va_arg(args, uint32_t), TO_STR);
                } else if(bin) {
                    dword_to_bin(va_arg(args, uint32_t), TO_STR);
                } else {
                    to_decimal((uint64_t)va_arg(args, uint32_t), TO_STR);
                }
            } else if(c == 'q') {
                if(hex && !bin) {
                    qword_to_hex(va_arg(args, uint64_t), TO_STR);
                } else if(bin) {
                    qword_to_bin(va_arg(args, uint64_t), TO_STR);
                } else {
                    to_decimal((uint64_t)va_arg(args, uint64_t), TO_STR);
                }
            } else {
                i--;
                if(hex && !bin) {
                    dword_to_hex(va_arg(args, uint32_t), TO_STR);
                } else if(bin) {
                    dword_to_bin((uint32_t)va_arg(args, uint32_t), TO_STR);
                } else {
                    to_decimal((uint64_t)va_arg(args, uint32_t), TO_STR);
                }
            }
            tmp = sputs(dst, TO_STR);
            dst += tmp;
        }
    }
    dst[0] = 0;
}