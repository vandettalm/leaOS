#ifndef _LINKED_LIST_H
#define _LINKED_LIST_H

#define LINKED_LIST(type, name) type name; name.___next = 0
#define LINKED_LIST_N(type, name) (name) = kalloc(sizeof(type)); (name)->___next = 0 
#define LINKED_LIST_START() typedef struct __attribute__((packed)) { void* ___next;
#define LINKED_LIST_END(name) } name
#define LINKED_LIST_NEXT(p) (p) = (p)->___next
#define LINKED_LIST_LAST(p) while((p)->___next != 0) {LINKED_LIST_NEXT((p));}
#endif