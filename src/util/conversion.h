#ifndef CONVERSION_H
#define CONVERSION_H

#include <stdint.h>

void byte_to_hex(uint8_t b, char* hex_out);
void word_to_hex(uint16_t w, char* hex_out);
void dword_to_hex(uint32_t d, char* hex_out);
void qword_to_hex(uint64_t q, char* hex_out);

void byte_to_bin(uint8_t b, char* bin_out);
void word_to_bin(uint16_t w, char* bin_out);
void dword_to_bin(uint32_t d, char* bin_out);
void qword_to_bin(uint64_t q, char* bin_out);

void to_decimal(uint64_t d, char* bin_out);

uint16_t byte_to_octal(uint8_t b);

#define HEX_STR(n) char n[17]
#define BIN_STR(n) char n[65]
#endif