#ifndef DISASM_H
#define DISASM_H

#include <stddef.h>
#include <stdint.h>

#include "../sys/dev/graphics/textmode/textmode_gfx.h"
#include "conversion.h"
#include "string.h"

typedef struct __attribute__((packed)) {
    uint8_t RM;
    uint8_t REG;
    uint8_t MOD;
} AMOSbyte;

typedef struct __attribute__((packed)) {
    uint8_t prefix_g1, prefix_g2, prefix_g3, prefix_g4;
    uint8_t opcode_1, opcode_2;
    uint8_t AMOS; //sometimes opcode_3
    uint8_t SIB;
    uint32_t disp;
    uint32_t im;

    uint8_t pfx;
    uint8_t via;
} asm_instruction;

void disasm_reset_state();

void disasm_decode_AMOS(uint8_t b, AMOSbyte* out);
void disasm_decode_AMOS_s(uint8_t b, AMOSbyte* out);

void disasm(uint32_t addr, uint32_t len);
void disasm_print();


/*

*/

// {"RETN", "LES", "LDS", "ENTER", "LEAVE", "RETF", "INT", "INTO", "IRET", "IRETD"};

#endif