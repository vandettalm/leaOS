#include"vmm.h"

extern uint32_t _pg_dir;
static uint32_t *page_dir;
static uint32_t page_tables[1024][1024]  __attribute__((aligned(0x1000)));

/*
    Pro Tip:
        Addresses in the page directories and tables are PHYSICAL, not linear.
*/

static void _vmm_flush_directory() {
    uint32_t tmp = (uint32_t)page_dir;
    __asm__ __volatile__("sub $0xC0000000, %0;"
                        "mov %0, %%cr3;"
                        :
                        : "r" (tmp)
                        :);
}

static bool _vmm_page_free(uint32_t v) {
    return ((page_tables[(v >> 22)][((v >> 12) & 0x3FF)] & 0x1) == 0x0);
}

static void _vmm_map_address(uint32_t p, uint32_t v, uint8_t f) {
    page_tables[(v >> 22)][((v >> 12) & 0x3FF)] = (uint32_t)((p & 0xFFFFF000) | f);
}

static uint32_t _vmm_get_address(uint32_t v) {
    return (page_tables[(v >> 22)][((v >> 12) & 0x3FF)] & 0xFFFFF000);
}

static void _vmm_free_page(uint32_t v, bool k) {
    if(!k && (v >= 0xC0000000)) {
        return;
    }
    mem_free_page((void*)_vmm_get_address(v));
    _vmm_map_address(0x00000000, v, 0x2);
}

static void* _vmm_request_pages(uint32_t n, bool k) {
    uint32_t count = 0, start = 0;
    bool is_free = false;
    bool is_fail = true;

    for(uint64_t i = ((k)?0xC0000000:0x00000000); i < ((k)?0xFFFFFFFF:0xC00000000); i += 0x1000) {
        is_free = _vmm_page_free(i);
        if(is_free && (count == 0)) {
            count++;
            start = i;
        } else if (is_free && (count != 0)) {
            count++;
        } else if(!is_free) {
            count = 0;
        }

        if(count == n) {
            is_fail = false;
            break;
        }
    }

    if(!is_fail) {
        for(uint32_t i = 0; i < n; i++) {
            uint32_t addr = (uint32_t)mem_request_page();
            _vmm_map_address(addr, (start + (i*0x1000)), 0x3);
        }
        return (void*)start;
    } else {
        return (void*)0;
    }
}

void vmm_init() {
    uint32_t tmp;
    page_dir = (uint32_t*)(((void*)(&_pg_dir)) + 0xC0000000);

    printf("%![VMM]%! Directory: 0x%hd\n", 0x0B, 0x09, page_dir);
    printf("%![VMM]%! Table: 0x%hd\n", 0x0B, 0x09, page_tables);
    printf("%![VMM]%! Populating Page Tables\n", 0x0B, 0x09);
    for(uint32_t dir = 0; dir < 1024; dir++) {
        for(uint32_t page = 0; page < 1024; page++) {
            tmp = ((dir << 22) | (page << 12));
            _vmm_map_address(tmp, tmp, 0x2);
        }
    }

    printf("%![VMM]%! Setting Up Kernel Space\n", 0x0B, 0x09);
    for(uint32_t i = 0x0; i <= (mem_last_init() - 0xC0000000); i += 0x1000) {
        _vmm_map_address(i, (i + 0xC0000000), 0x3);
    }

    printf("%![VMM]%! Mapping Text Mode Buffer\n", 0x0B, 0x09);
    for(uint32_t i = 0; i < 8; i++) {
        _vmm_map_address((0xb8000 + (i*0x1000)), (0xb8000 + (i*0x1000)), 0x3);
    }

    printf("%![VMM]%! Modifying Page Directories\n", 0x0B, 0x09);
    for(uint32_t i = 0; i < 1024; i++) {
        page_dir[i] = (uint32_t)((((uint32_t)page_tables[i]) - 0xC0000000) | 0x3); 
    }

    printf("%![VMM]%! Flushing Page Directories\n", 0x0B, 0x09);
    _vmm_flush_directory();
}

void *vmm_request_page() {
    return _vmm_request_pages(1, false);
}

void *vmm_request_kpage() {
    return _vmm_request_pages(1, true);
}

void *vmm_request_pages(uint32_t n) {
    return _vmm_request_pages(n, false);
}

void *vmm_request_kpages(uint32_t n) {
    return _vmm_request_pages(n, true);
}

void vmm_free_page(void *v) {
    _vmm_free_page((uint32_t)v, false);
}

void vmm_free_kpage(void *v) {
    _vmm_free_page((uint32_t)v, true);
}

void vmm_flush() {
    _vmm_flush_directory();
}

uint32_t vmm_translate_address(uint32_t v) {
    return (v & 0xFFF) | _vmm_get_address(v);
}