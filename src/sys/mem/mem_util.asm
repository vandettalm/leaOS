section .text

global memset:function (memset.end - memset)
global memsetw:function (memsetw.end - memsetw)
global memcpy:function (memcpy.end - memcpy)
global outb:function (outb.end - outb)
global outw:function (outw.end - outw)
global outd:function (outd.end - outd)
global ind:function (ind.end - ind)
global inw:function (inw.end - inw)
global inb:function (inb.end - inb)

memset:
    push ebp
    mov ebp, esp
    push edi

    mov edi, [ebp+8]
    mov eax, [ebp+12]
    mov ecx, [ebp+16]
    rep stosb

    pop edi
    leave
    ret
    .end:

memsetw:
    push ebp
    mov ebp, esp
    push edi

    mov edi, [ebp+8]
    mov eax, [ebp+12]
    mov ecx, [ebp+16]
    rep stosw

    pop edi
    leave
    ret
    .end:

memcpy:
    push ebp
    mov ebp, esp
    push edi
    push esi

    mov edi, [ebp+8]
    mov esi, [ebp+12]
    mov ecx, [ebp+16]
    rep movsb

    pop esi
    pop edi
    leave
    ret
    .end:


outb:
    push ebp
    mov ebp, esp

    mov edx, [ebp+12]
    mov eax, [ebp+8]
    out dx, al

    leave
    ret
    .end:

outw:
    push ebp
    mov ebp, esp

    mov edx, [ebp+12]
    mov eax, [ebp+8]
    out dx, ax

    leave
    ret
    .end:

outd:
    push ebp
    mov ebp, esp

    mov edx, [ebp+12]
    mov eax, [ebp+8]
    out dx, eax

    leave
    ret
    .end:

inb:
    push ebp
    mov ebp, esp

    mov edx, [ebp+8]
    xor eax, eax
    in al, dx

    leave
    ret
    .end:

inw:
    push ebp
    mov ebp, esp

    mov edx, [ebp+8]
    xor eax, eax
    in ax, dx

    leave
    ret
    .end:

ind:
    push ebp
    mov ebp, esp

    mov edx, [ebp+8]
    in eax, dx

    leave
    ret
    .end: