#ifndef VMM_H
#define VMM_H

#include <stdint.h>
#include <stdbool.h>

#include"../dev/graphics/textmode/textmode_gfx.h"
#include"mem_section.h"

void vmm_init();

void* vmm_request_page();
void* vmm_request_pages(uint32_t n);
void* vmm_request_kpage();
void* vmm_request_kpages(uint32_t n);

void vmm_free_page(void *v);
void vmm_free_kpage(void *v);

void vmm_flush();

uint32_t vmm_translate_address(uint32_t v);

#endif