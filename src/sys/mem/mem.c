#include "mem.h"

static alloc_header *kalloc_header = (void*)0xFFFFFFFF;

static void alloc_header_init(alloc_header *p, uint32_t b, uint32_t n) {
    p->base = b;
    p->num  = n;
    p->used = 0;
    p->___next = (void*)0xFFFFFFFF;
}

static alloc_header *alloc_header_get_last(alloc_header *h) {
    while(h->___next != (void*)0xFFFFFFFF) {
        LINKED_LIST_NEXT(h);
    }
    return h;
}

/* UNUSED ATM
static alloc_header *alloc_struct_get_next_data(alloc_header* h) {
    if(h->___next == (void*)0xFFFFFFFF) {
        return 0;
    } else {
        do {
            LINKED_LIST_NEXT(h);
        } while(h->num == 0);
        return h;
    }
}
*/

static alloc_header *alloc_header_get_free(alloc_header* h) {
    do {
        if(h->num == 0) {
            if((0x1000 - h->used) >= sizeof(alloc_header)) {
                return h;
            }
        }
        LINKED_LIST_NEXT(h);
    } while(h != (void*)0xFFFFFFFF);
    return (void*)0xFFFFFFFF;
}

static bool alloc_prefix_is_used(alloc_prefix *p) {
    return ((p->flags & 0x1) == 1);
}

static bool alloc_prefix_is_last(alloc_prefix *p) {
    return (((p->flags >> 1) & 0x1) == 1);
}

static alloc_prefix *alloc_prefix_find_free(alloc_prefix* p, uint32_t n) {
    if(!alloc_prefix_is_used(p)) {
        if(p->size >= n) {
            return p;
        } else {
            if(!alloc_prefix_is_last(p)) {
                return alloc_prefix_find_free((((void*)p) + sizeof(alloc_prefix) + p->size), n);
            } else {
                return 0;
            }
        }
    } else {
        if(!alloc_prefix_is_last(p)) {
            return alloc_prefix_find_free((((void*)p) + sizeof(alloc_prefix) + p->size), n);
        } else {
            return 0;
        }
    }
}

static alloc_prefix *alloc_prefix_next(alloc_prefix* p) {
    void *o = p;
    o += sizeof(alloc_prefix) + p->size;
    return o;
}

static alloc_prefix *alloc_prefix_get_last(alloc_prefix* p) {
    while(!alloc_prefix_is_last(p)) {
        p = alloc_prefix_next(p);
    }
    return p;
}

void *kalloc(uint32_t size) {
    alloc_header *header = 0;
    alloc_prefix *prefix = 0;

    if(kalloc_header == (void*)0xFFFFFFFF) {
        kalloc_header = vmm_request_kpage();
        vmm_flush();
        alloc_header_init(kalloc_header, (uint32_t)kalloc_header, 0);
        kalloc_header->used = sizeof(alloc_header);
    }

    for(alloc_header *h = kalloc_header; h != (void*)0xFFFFFFFF; LINKED_LIST_NEXT(h)) {
        if(h->num != 0) {
            if(((h->num * 0x1000) - h->used) >= (size + sizeof(alloc_prefix))) {
                prefix = (void*)(h->base);

                if(h->used == 0) {
                    h->used += sizeof(alloc_prefix) + size;
                    prefix->flags = 0x1 | 0x2 | (size >= (0x1000 - sizeof(alloc_prefix)))?0x4:0x0;
                    prefix->size = size;
                    return (((void*)prefix) + sizeof(alloc_prefix));
                } else {
                    prefix = alloc_prefix_find_free(prefix, size);

                    if(prefix != 0) {
                        prefix->flags = 0x1 | 0x2 | (size >= (0x1000 - sizeof(alloc_prefix)))?0x4:0x0;
                        h->used += sizeof(alloc_prefix) + prefix->size;
                        return (((void*)prefix) + sizeof(alloc_prefix));
                    } else {
                        h->used += (sizeof(alloc_prefix) + size);
                        prefix = alloc_prefix_get_last((void*)(h->base));
                        prefix->flags &= 0xFFFD;
                        prefix = alloc_prefix_next(prefix);
                        prefix->size = size;
                        prefix->flags = (0x1 | 0x2 | ((size >= (0x1000 - sizeof(alloc_prefix)))?0x4:0x0));
                        return (void*)(((uint32_t)prefix) + sizeof(alloc_prefix));
                    }
                }
            }
        }
    }

    header = alloc_header_get_free(kalloc_header);
    alloc_header *last = alloc_header_get_last(kalloc_header);

    if(header == (void*)0xFFFFFFFF) {
        last->___next = vmm_request_kpage();
        vmm_flush();
        header = last->___next;
        LINKED_LIST_NEXT(last);
        alloc_header_init(header, (uint32_t)header, 0);
    }

    last->___next = (void*)(header->base + header->used);
    LINKED_LIST_NEXT(last);
    header->used += sizeof(alloc_header) + size;
    uint32_t p = (size/0x1000);

    if(size > 0x1000) {
        p += (size%0x1000 == 0)?0:1;
    } else {
        p = 1;
    }

    alloc_header_init(last, (uint32_t)vmm_request_kpages(p), p);
    last->used = size + sizeof(alloc_prefix);
    vmm_flush();
    prefix = (void*)last->base;
    prefix->flags = (0x1 | 0x2 | ((size >= (0x1000 - sizeof(alloc_prefix)))?0x4:0x0));
    prefix->size = size;

    return (void*)(last->base + sizeof(alloc_prefix)); 
}

