#ifndef MEM_SECTION_H
#define MEM_SECTION_H

#include <stdint.h>
#include <stdbool.h>

#include "../dev/graphics/textmode/textmode_gfx.h"
#include "../../util/linked_list.h"
#include "mem_util.h"

typedef struct __attribute__((packed)) {
    uint32_t enc;
    uint64_t base;
    uint64_t size;
    uint32_t type;
} bios_mem_map;

LINKED_LIST_START();
    uint32_t base;
    uint32_t size;
    uint8_t *bitmap;
LINKED_LIST_END(mem_section);

void mem_init(void* start_addr, uint64_t mmap);
void* mem_request_page();
void mem_free_page(void* addr);
uint32_t mem_last_init();

#endif