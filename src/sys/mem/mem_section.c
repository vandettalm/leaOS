#include"mem_section.h"

/* UNUSED ATM
static const char *bios_mem_type[] = {"Error", "Usable", "Reserved", "ACPI Reclaim", "ACPI NVS", "Bad Memory"};
*/
static uint32_t current_offset = 0;
static uint32_t total_mem = 0;
static uint32_t last_used = 0;
static mem_section *memory_map;

static mem_section *get_index(uint32_t i) {
    mem_section* tmp = memory_map;
    while(i != 0) {
        LINKED_LIST_NEXT(tmp);
        i--;
    }
    return tmp;
}

/*
static bool mem_get_page(uint32_t addr) {
    mem_section *tmp;
    uint8_t *tmp2;
    for(uint32_t i = 0; i < current_offset; i++) {
        tmp = get_index(i);

        if((tmp->base <= addr) && (addr < (tmp->base + tmp->size))) {
            addr -= tmp->base; // address relative to base
            addr /= 4096; // page number
            tmp2 = &(tmp->bitmap[addr/8]);
            return ((tmp2[0] & (0x1 << (addr%8))) == 1);
        }
    }

    return false;
}
*/

static void mem_set_page(uint32_t addr, bool v) {
    mem_section *tmp;
    uint32_t tmp3 = addr;

    for(uint32_t i = 0; i <= current_offset; i++) {
        tmp = get_index(i);

        if((tmp->base <= addr) && (addr < (tmp->base + tmp->size))) {
            uint8_t k;
            uint32_t j, c;

            tmp3 -= tmp->base;
            tmp3 /= 0x1000;
            j = tmp3 / 0x8;
            c = tmp3 - (j*0x8);

            k = tmp->bitmap[j];
            k &= ~(0x1 << c);

            if(v) {
                k |= (0x1 << c);
            }

            tmp->bitmap[j] = k;
            return;
        }
    }
}

void mem_init(void* start_addr, uint64_t mmap) {
    mem_section *tmp;
    memory_map = start_addr;
    bios_mem_map *mmap_addr = (void*)((uint32_t)(mmap & 0x00000000FFFFFFFF));
    uint32_t mmap_len = ((uint32_t)((mmap & 0xFFFFFFFF00000000)>>32))/24;

    for(uint32_t i = 0; i < mmap_len; i++) {
        //printf("%![MEM]%!   0x%hd -> %s\n", 0x0D, 0x0B, mmap_addr, bios_mem_type[mmap_addr->type]);
        //printf("%![MEM]%! \tBase: 0x%hq\n", 0x0D, 0x0B, mmap_addr->base);
        //printf("%![MEM]%! \tSize: 0x%hq\n", 0x0D, 0x0B, mmap_addr->size);

        if(mmap_addr->type == 1) {
            printf("%![MEM]%! Found %id Byte memory block\n", 0x0D, 0x0B, mmap_addr->size);
            tmp = get_index(current_offset);
            tmp->base = (uint32_t)mmap_addr->base;
            tmp->size = (uint32_t)mmap_addr->size;
            tmp->bitmap = ((void*)tmp) + sizeof(mem_section);
            for(uint32_t j = 0; j < ((tmp->size/4096))/8; j++) {
                tmp->bitmap[j] = 0x00;
            }
            total_mem += ((tmp->size/4096))/8;
            last_used = (uint32_t)(tmp->bitmap + (((tmp->size/4096))/8));
            tmp->___next = (void*)last_used;
            current_offset++;
        }
        mmap_addr++;
    }

    current_offset--;
    get_index(current_offset)->___next = 0x00000000;
    total_mem *= (8*4096);
    printf("%![MEM]%! Total Addressable Memory: %id Bytes\n", 0x0D, 0x0B, total_mem);

    for(uint32_t i = 0x100000; i < (last_used - 0xC0000000); i += 4096) {
        mem_set_page(i, true);
    }   

    for(uint32_t i = 0x0; i < 8; i++) {
        mem_set_page(0xb8000 + (i*0x1000), true);
    }
}

void *mem_request_page() {
    mem_section *tmp;
    for(uint32_t i = 0; i <= current_offset; i++) {
        tmp = get_index(i);

        for(uint32_t j = 0; j < ((tmp->size/4096))/8; j++) {
            if(tmp->bitmap[j] != 0xFF) {
                uint8_t t = tmp->bitmap[j];
                uint8_t c = 0;

                t = ~t;
                while((t & 0x1) != 1) {
                    c++;
                    t = t >> 1;
                }

                tmp->bitmap[j] |= (0x1 << c);
                //printf("%![MEM]%! Allocating Block 0x%hd\n", 0x0D, 0x0B, (tmp->base + (4096*((j*8) + c))));
                return (void*)(tmp->base + (4096*((j*8) + c)));
            }
        }
    }
    return (void*)0;
}

void mem_free_page(void* addr) {
    mem_set_page((uint32_t)addr, false);
    //printf("%![MEM]%! Freeing Block 0x%hd\n", 0x0D, 0x0B, addr);
}

uint32_t mem_last_init() {
    return last_used;
}