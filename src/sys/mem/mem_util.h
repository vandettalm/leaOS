#ifndef MEM_UTIL_H
#define MEM_UTIL_H

#include<stdint.h>

extern void memset(void *dst, uint8_t v, uint32_t size);
extern void memsetw(void *dst, uint16_t v, uint32_t size);
extern void memcpy(void *dst, void *src, uint32_t size);

extern void outb(uint8_t v, uint16_t p);
extern void outw(uint16_t v, uint16_t p);
extern void outd(uint32_t v, uint16_t p);

extern uint8_t inb(uint16_t p);
extern uint16_t inw(uint16_t p);
extern uint32_t ind(uint16_t p);

#define in(p) (inb(p))
#define out(v,p) (outb(v,p))

#endif