#ifndef _MEM_H
#define _MEM_H

#include <stdint.h>
#include <stdbool.h>

#include"../dev/graphics/textmode/textmode_gfx.h"
#include "../../util/linked_list.h"
#include"vmm.h"

LINKED_LIST_START();
    uint32_t base;
    uint32_t num;
    uint32_t used;
LINKED_LIST_END(alloc_header);

typedef struct __attribute__((packed)) {
    uint16_t flags;
    uint32_t size;
} alloc_prefix;

/*
    Flags: 0000000000000MLU
             1  |  0
       U:  Used | Unused
       L:  Last | In Chain
       M: >page | <page 
*/

void *kalloc(uint32_t size);

#endif