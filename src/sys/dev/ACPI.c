#include"ACPI.h"
#include"FADT.h"

static uint8_t bda[256];
static uint8_t *ebda_addr;
static rsdp_desc2 *rsdp;
static acpi_sdt *first_header;
static uint32_t sdt_entries;

static acpi_fadt *fadt;

void save_bda() {
    memcpy(&bda, (void*)0x400, 256);
    ebda_addr = (void*)((((uint16_t*)bda)[7]) << 4);
    //printf("%![ACPI]%! EBDA Address: 0x%hd\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), ebda_addr);
}

void load_rsdp() {
    bool found = false;
    uint8_t* ptr = ebda_addr;

    for(uint32_t i = 0; i < 1024; i++) {
        if((*(uint64_t*)(ptr + i)) == 0x2052545020445352) {
            found = true;
            ptr += i;
            break;
        }
    }
    
    if(!found) {
        for(ptr = (uint8_t*)0x000E0000; ptr < (uint8_t*)0x000FFFFF; ptr++) {
            if((*(uint64_t*)(ptr)) == 0x2052545020445352) {
                found = true;
                break;
            }
        }
    }

    if(found) {
        rsdp = (void*)ptr;
        printf("%![ACPI]%! Found RSDP: 0x%hd\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), rsdp);
        printf("%![ACPI]%!   Sig Check: %c%c%c%c%c%c%c%c\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), rsdp->first.sig[0], rsdp->first.sig[1], rsdp->first.sig[2], rsdp->first.sig[3], rsdp->first.sig[4], rsdp->first.sig[5], rsdp->first.sig[6], rsdp->first.sig[7]);
        printf("%![ACPI]%!   OEM: %c%c%c%c%c%c\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), rsdp->first.oem[0], rsdp->first.oem[1], rsdp->first.oem[2], rsdp->first.oem[3], rsdp->first.oem[4], rsdp->first.oem[5]);
        printf("%![ACPI]%!   RSDT: 0x%hd\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), rsdp->first.rsdt_addr);
        printf("%![ACPI]%!   ACPI Version: %s (%ib)\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), ((rsdp->first.rev == 0) ? "1.0":">2.0") , rsdp->first.rev);
    } else {
        printf("Error: Could not find RSPD\n");
    }
}

void load_sdt() {
    acpi_sdt* tmp;
    first_header = (void*)rsdp->first.rsdt_addr;
    sdt_entries = (first_header->len - sizeof(acpi_sdt))/4;

    printf("%![ACPI]%! SDT: %c%c%c%c\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), first_header->sig[0], first_header->sig[1], first_header->sig[2], first_header->sig[3]);
    printf("%![ACPI]%!   Entries: %id\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), sdt_entries);

    for(uint32_t i = 0; i < sdt_entries; i++) {
        tmp = (void*)(*(uint32_t*)(((void*)first_header) + sizeof(acpi_sdt) + (4*i)));
        printf("%![ACPI]%! \t#%id: %c%c%c%c - %c%c%c%c%c%c - %c%c%c%c%c%c%c%c\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_GREEN), i, tmp->sig[0], tmp->sig[1], tmp->sig[2], tmp->sig[3], 
            tmp->oem[0],tmp->oem[1],tmp->oem[2],tmp->oem[3],tmp->oem[4],tmp->oem[5],
            tmp->table_id[0],tmp->table_id[1],tmp->table_id[2],tmp->table_id[3],tmp->table_id[4],tmp->table_id[5],tmp->table_id[6],tmp->table_id[7]);
        
        if(strncmp((const char*)(tmp->sig), "FACP", 4)) {
            load_fadt((void*)tmp);
        }
    }
}

void load_fadt(void *addr) {
    fadt = addr;
    printf("%![ACPI]%! \t\tDSDT Addr - 0x%hd\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_VIOLET), fadt->dsdt_addr);
    printf("%![ACPI]%! \t\tPPMP - %ib\n", FG_COLOR(COLOR_BR_BLUE), FG_COLOR(COLOR_BR_VIOLET), fadt->prefered_pmp);
    load_dsdt((void*)fadt->dsdt_addr);
}

void load_dsdt(void *addr) {
    //acpi_sdt *header = addr;
    uint8_t *AML = addr + sizeof(acpi_sdt);

    printf("Lead Byte: 0x%hb\n", AML[0]);
    printf("Length Encoding Size: %ib\n", (AML[1] & 0x3));

    switch((AML[1] & 0x3)) {
        case 1:
            printf("Length: %ib\n", AML[1]);
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            break;
    }
}