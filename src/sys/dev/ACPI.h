#ifndef ACPI_H
#define ACPI_H

#include <stdint.h>
#include <stdbool.h>

#include"graphics/textmode/textmode_gfx.h"
#include"../mem/mem_util.h"
#include"../../util/string.h"

typedef struct __attribute__((packed)) {
    char sig[8];
    uint8_t checksum;
    char oem[6];
    uint8_t rev;
    uint32_t rsdt_addr;
} rsdp_desc;

typedef struct __attribute__((packed)) {
    rsdp_desc first;

    uint32_t len;
    uint64_t xsdt_addr;
    uint8_t ext_checksum;
    uint8_t resv[3];
} rsdp_desc2;

typedef struct __attribute__((packed)) {
    char sig[4];
    uint32_t len;
    uint8_t rev;
    uint8_t checksum;
    char oem[6];
    char table_id[8];
    uint32_t oem_rev;
    uint32_t creator_id;
    uint32_t creator_rev;
} acpi_sdt;

typedef struct __attribute__((packed)) {
    uint8_t addr_space;
    uint8_t bit_width;
    uint8_t bit_offset;
    uint8_t access_size;
    uint64_t address;
} acpi_gas;

void save_bda();
void load_rsdp();
void load_sdt();

void load_fadt(void *addr);
void load_dsdt(void *addr);

#endif