#ifndef _PCI_H
#define _PCI_H

#include<stdint.h>
#include<stdbool.h>
#include"pci_def.h"
#include"../graphics/textmode/textmode_gfx.h"
#include"../../mem/mem.h"
#include"../../mem/mem_util.h"

#define PCI_ADDR_DWORD(b,d,f,r) (0x80000000 | (((uint32_t)(b)) << 16) | (((uint32_t)((d) & 0x1F)) << 11) | (((uint32_t)((f) & 0x07)) << 8) | ((uint32_t)(r)&0xFC) )
#define PCI_ADDR_BUS(a) ((uint8_t)(((a)>>16)&0xFF))
#define PCI_ADDR_DEV(a) ((uint8_t)(((a)>>11)&0x1F))
#define PCI_ADDR_FUN(a) ((uint8_t)(((a)>>8)&0x07))
#define PCI_CONFIG_ADDRESS 0x0CF8
#define PCI_CONFIG_DATA 0x0CFC

extern const char* PCI_CLASS_STRING[18];

typedef struct __attribute__((packed)) {
    uint16_t vendor;
    uint16_t device;
    uint16_t command;
    uint16_t status;

    uint8_t revision;
    uint8_t prog;
    uint8_t subclass;
    uint8_t class;
    
    uint8_t cache_line_size;
    uint8_t latency;
    uint8_t header_type;
    uint8_t bist;
} _pci_base_header;

typedef struct __attribute__((packed)) {
    uint32_t bar0, bar1, bar2, bar3, bar4, bar5;
    uint32_t cb_cis_ptr;
    uint16_t s_vender;
    uint16_t s_sys;
    uint32_t exp_rom;
    uint8_t c_ptr;
    uint8_t resv1;
    uint16_t resv2;
    uint32_t resv3;
    uint8_t int_line;
    uint8_t int_pin;
    uint8_t min_grant;
    uint8_t min_latency;
} _pci_header_type00;

typedef struct __attribute__((packed)) {
    uint32_t bar0, bar1;
    uint8_t pri_bus_num, sec_bus_num, sub_bus_num, sec_latency_timer;
    uint8_t io_base;
    uint8_t io_limit;
    uint16_t sec_status;
    uint16_t mem_base;
    uint16_t mem_limit;
    uint16_t prefetch_base;
    uint16_t prefetch_limit;
    uint32_t prefetch_base_upper;
    uint32_t prefetch_limit_upper;
    uint16_t io_base_upper;
    uint16_t io_limit_upper;
    uint8_t cap_ptr;
    uint8_t resv1;
    uint16_t resv2;
    uint32_t exp_rom_addr;
    uint8_t int_line;
    uint8_t int_pin;
    uint16_t bridge_ctrl;
} _pci_header_type01;

typedef struct __attribute__((packed)) {
    uint32_t cb_soc_base_addr;
    uint8_t offset_cap_list;
    uint8_t resv1;
    uint16_t sec_status;
    uint8_t pci_bus_num;
    uint8_t cb_bus_num;
    uint8_t sub_bus_num;
    uint8_t cb_latency_timer;
    uint32_t mem_base_addr0;
    uint32_t mem_limit0;
    uint32_t mem_base_addr1;
    uint32_t mem_limit1;
    uint32_t io_base_addr0;
    uint32_t io_limit0;
    uint32_t io_base_addr1;
    uint32_t io_limit1;
    uint8_t int_line;
    uint8_t int_pin;
    uint16_t bridge_ctrl;
    uint16_t sub_dev_id;
    uint16_t sub_vender;
    uint32_t legacy_addr;
} _pci_header_type02;

typedef struct __attribute__((packed)) {
    _pci_base_header base;
    union {
        _pci_header_type00 type0;
        _pci_header_type01 type1;
        _pci_header_type02 type2;
    };
    uint32_t address;
} pci_header;


LINKED_LIST_START();
    pci_header header;
    void* children;
LINKED_LIST_END(pci_device_tree);

const char* pci_get_device_class(pci_header* h);
const char* pci_get_device_type(pci_header* h);

void pci_scan_devices();
void pci_dump();

pci_header* pci_find_class(uint8_t class);
pci_header* pci_find_subclass(uint8_t class, uint8_t sub);
pci_header* pci_find_next_class(pci_header* last);
pci_header* pci_find_next_subclass(pci_header* last);


#endif