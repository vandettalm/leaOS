#ifndef PCI_DEF_H
#define PCI_DEF_H

#define PCI_CLASS_LEGACY        0x0
#define PCI_CLASS_MASS_STORAGE  0x1
#define PCI_CLASS_NETWORK       0x2
#define PCI_CLASS_DISPLAY       0x3
#define PCI_CLASS_MULTIMEDIA    0x4
#define PCI_CLASS_MEMORY        0x5
#define PCI_CLASS_BRIDGE        0x6
#define PCI_CLASS_COM           0x7
#define PCI_CLASS_BASE_PERIPH   0x8
#define PCI_CLASS_INPUT         0x9
#define PCI_CLASS_DOCK          0xA
#define PCI_CLASS_CPU           0xB
#define PCI_CLASS_SERIAL_BUS    0xC
#define PCI_CLASS_WIRELESS      0xD
#define PCI_CLASS_INTEL_IO      0xE
#define PCI_CLASS_SAT_COM       0xF
#define PCI_CLASS_CRYPTO        0x10
#define PCI_CLASS_DATA_PROC     0x11


//Mass Storage
#define PCI_MAST_SCSI           0x0
#define PCI_MAST_IDE            0x1
#define PCI_MAST_FLOPPY         0x2
#define PCI_MAST_IPI            0x3
#define PCI_MAST_RAID           0x4
#define PCI_MAST_ATA            0x5
#define PCI_MAST_SATA           0x6
#define PCI_MAST_SAS            0x7
#endif