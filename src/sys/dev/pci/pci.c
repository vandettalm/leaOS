#include"pci.h"

static pci_device_tree *PCI_TREE = 0;

const char* PCI_CLASS_STRING[18] = {
    "Legacy",                       //0x0
    "Mass Storage Controller",      //0x1
    "Network Controller",           //0x2
    "Display Controller",           //0x3
    "Multimedia Controller",        //0x4
    "Memory Controller",            //0x5
    "Bridge Device",                //0x6
    "Simple Com Controller",        //0x7
    "Base Peripheral",              //0x8
    "Input Device",                 //0x9
    "Docking Station",              //0xA
    "CPU",                          //0xB
    "Serial Bus Controller",        //0xC
    "Wireless Controller",          //0xD
    "Intelligent I/O Controller",   //0xE
    "SAT Com Controller",           //0xF
    "Crypto Controller",            //0x10
    "DAC/Signal Processing Controller" //0x11
};

static bool pci_is_multifunc(pci_header* h) {
    return ((h->base.header_type & 0x80) != 0);
}

const char* pci_get_device_class(pci_header* h) {
    uint8_t class = h->base.class;
    if(class <= 0x11) {
        return PCI_CLASS_STRING[class];
    }
    return "<Unknown>";
}

const char* pci_get_device_type(pci_header* h) {
    uint8_t class = h->base.class;
    uint8_t subclass = h->base.subclass;
    uint8_t progif = h->base.prog;

    switch(class) {
        case 0x0:
            return (subclass == 0)?"Non-VGA Device":"VGA-Compatible Device";
        case 0x1:
            switch(subclass) {
                case 0x0:
                    return "SCSI Controller";
                case 0x1:
                    return "IDE Controller";
                case 0x2:
                    return "Floppy Disk Controller";
                case 0x3:
                    return "IPI Bus Controller";
                case 0x4:
                    return "RAID Controller";
                case 0x5:
                    switch(progif) {
                        case 0x20:
                            return "ATA Controller (Single)";
                        case 0x30:
                            return "ATA Controller (Chained)";
                        default:
                            return "ATA Controller";
                    }
                case 0x6:
                    switch(progif) {
                        case 0x01:
                            return "Serial ATA (AHCI 1.0)";
                        default:
                            return "Serial ATA";
                    }
                case 0x7:
                    return "SAS Controller";
                case 0x80:
                    return "Other Mass Storage Controller";
                default:
                    return "Unknown Mass Storage Controller";
            }
        case 0x2:
            switch(subclass) {
                case 0x00:
                    return "Ethernet Controller";
                case 0x01:
                    return "Token Ring Controller";
                case 0x02:
                    return "FDDI Controller";
                case 0x03:
                    return "ATM Controller";
                case 0x04:
                    return "ISDN Controller";
                case 0x05:
                    return "WorldFip Controller";
                case 0x06:
                    return "PICMG MultiComp";
                case 0x80:
                    return "Other Network Controller";
                default:
                    return "Unknown Network Controller";
            }
        case 0x3:
            switch(subclass) {
                case 0x00:
                    switch(progif) {
                        case 0x00:
                            return "VGA Compatible Controller";
                        case 0x01:
                            return "8512 Compatible Controller";
                        default:
                            return "Unknown Display Controller";
                    }
                case 0x01:
                    return "XGA Controller";
                case 0x02:
                    return "Non-VGA 3D Controller";
                case 0x80:
                    return "Other Display Controller";
                default:
                    return "Unknown Display Controller";
            }
        case 0x4:
            switch(subclass) {
                case 0x00:
                    return "Video Device";
                case 0x01:
                    return "Audio Device";
                case 0x02:
                    return "Telephony Device";
                case 0x80:
                    return "Other Multimedia Device";
                default:
                    return "Unknown Multimedia Device";
            }
        case 0x5:
            switch(subclass) {                              
                case 0x00:
                    return "RAM Controller";
                case 0x01:
                    return "Flash Controller";
                case 0x80:
                    return "Other Memory Controller";
                default:
                    return "Unknown Memory Controller";
            }
        case 0x6:
            switch(subclass) {                              
                case 0x00:
                    return "Host Bridge";
                case 0x01:
                    return "ISA Bridge";
                case 0x02:
                    return "EISA Bridge";
                case 0x03:
                    return "MCA Bridge";
                case 0x04:
                    switch(progif) {
                        case 0x00:
                            return "PCI->PCI Bridge";
                        case 0x01:
                            return "PCI->PCI Bridge";
                        default:
                            return "Unknown PCI->PCI Bridge";
                    }
                case 0x05:
                    return "PCMCIA Bridge";
                case 0x06:
                    return "NuBus Bridge";
                case 0x07:
                    return "CardBus Bridge";
                case 0x08:
                    return "RACEway Bridge";
                case 0x09:
                    switch(progif) {
                        case 0x40:
                            return "Primary PCI->PCI Transparent";
                        case 0x80:
                            return "Secondary PCI->PCI Transparent";
                        default:
                            return "Unknown Transparent PCI->PCI";
                    }
                case 0x0a:
                    return "InfiniBrand->PCI Host Bridge";
                case 0x80:
                    return "Other Bridge Device";
                default:
                    return "Unknown Bridge Device"; 
            }
        case 0x7:
            switch(subclass) {                              
                case 0x00:
                    switch(progif) {
                        case 0x00:
                            return "Generic XT Compatible Serial";
                        case 0x01:
                            return "16450 Compatible Serial";
                        case 0x02:
                            return "16550 Compatible Serial";
                        case 0x03:
                            return "16650 Compatible Serial";
                        case 0x04:
                            return "16750 Compatible Serial";
                        case 0x05:
                            return "16850 Compatible Serial";
                        case 0x06:
                            return "16950 Compatible Serial";
                        default:
                            return "Unknown Serial";
                    }
                case 0x01:
                    switch(progif) {
                        case 0x00:
                            return "Parallel Port";
                        case 0x01:
                            return "Bi-Parallel Port";
                        case 0x02:
                            return "ECP 1.X Parallel Port";
                        case 0x03:
                            return "IEEE 1284 Controller";
                        case 0xFE:
                            return "IEEE 1284 Target";
                        default:
                            return "Unknown Parallel";
                    }
                case 0x02:
                    return "Multi-Port Serial Controller";
                case 0x03:
                    switch(progif) {
                        case 0x00:
                            return "Generic Modem";
                        case 0x01:
                            return "16450 Compatible Modem";
                        case 0x02:
                            return "16550 Compatible Modem";
                        case 0x03:
                            return "16650 Compatible Modem";
                        case 0x04:
                            return "16750 Compatible Modem";
                        default:
                            return "Unknown Modem";
                    }
                case 0x04:
                    return "IEEE 488.1/2 Controller";
                case 0x05:
                    return "Smart Card";
                case 0x80:
                    return "Other Coms Device";
                default:
                    return "Unknown Coms Device";
            }
        case 0x8:
            switch(subclass) {                              
                case 0x00:
                    switch(progif) {
                        case 0x00:
                            return "8259 PIC";
                        case 0x01:
                            return "ISA PIC";
                        case 0x02:
                            return "EISA PIC";
                        case 0x10:
                            return "I/O APIC";
                        case 0x20:
                            return "I/Ox APIC";
                        default:
                            return "Unknown PIC";
                    }
                case 0x01:
                    switch(progif) {
                        case 0x00:
                            return "8237 DMA Controller";
                        case 0x01:
                            return "ISA DMA Controller";
                        case 0x02:
                            return "EISA DMA Controller";
                        default:
                            return "Unknown DMA Controller";
                    }
                case 0x02:
                    switch(progif) {
                        case 0x00:
                            return "8254 System Timer";
                        case 0x01:
                            return "ISA System Timer";
                        case 0x02:
                            return "EISA System Timer";
                        default:
                            return "Unknown System Timer";
                    }
                case 0x03:
                    switch(progif) {
                        case 0x00:
                            return "RTC Controller";
                        case 0x01:
                            return "ISA RTC Controller";
                        default:
                            return "Unknown RTC Controller";
                    }
                case 0x04:
                    return "PCI Hot-Plug Controller";
                case 0x80:
                    return "Other System Peripheral";
                default:
                    return "Unknown System Peripheral";
            }
        case 0x9:
            switch(subclass) {                             
                case 0x00:
                    return "Keyboard Controller";
                case 0x01:
                    return "Digitizer";
                case 0x02:
                    return "Mouse Controller";
                case 0x03:
                    return "Scanner Controller";
                case 0x04:
                    switch(progif) {
                        case 0x00:
                            return "Gameport Controller";
                        case 0x10:
                            return "Legacy Gameport Controller";
                        default:
                            return "Unknown Gameport Controller";
                    }
                case 0x80:
                    return "Other Input Controller";
                default:
                    return "Unknown Input Controller";
            }
        case 0xa:
            switch(subclass) {                             
                case 0x00:
                    return "Docking Station";
                case 0x80:
                    return "Other Docking Station";
                default:
                    return "Unknown Docking Station";
            }
        case 0xb:
            switch(subclass) {                            
                case 0x00:
                    return "386 Processor";
                case 0x01:
                    return "486 Processor";
                case 0x02:
                    return "Pentium Processor";
                case 0x10:
                    return "Alpha Processor";
                case 0x20:
                    return "PowerPC Processor";
                case 0x30:
                    return "MIPS Processor";
                case 0x40:
                    return "Co-Processor";
                default:
                    return "Unknown Processor";
            }
        case 0xc:
            switch(subclass) {                           
                case 0x00:
                    switch(progif) {
                        case 0x00:
                            return "FireWire IEEE 1394 Controller";
                        case 0x10:
                            return "OpenHCL IEEE 1394 Controller";
                        default:
                            return "Unknown IEEE 1394 Controller";
                    }
                case 0x01:
                    return "ACCESS Bus";
                case 0x02:
                    return "SSA";
                case 0x03:
                    switch(progif) {
                        case 0x00:
                            return "Universal USB Host Controller";
                        case 0x10:
                            return "Open USB Host Controller";
                        case 0x20:
                            return "USB2 Host Controller";
                        case 0x30:
                            return "XHCl USB3 Controller";
                        case 0x80:
                            return "Other USB Controller";
                        case 0xFE:
                            return "Non-Host USB";
                        default:
                            return "Unknown USB Controller";
                    }
                case 0x04:
                    return "Fibre Channel";
                case 0x05:
                    return "SMBus";
                case 0x06:
                    return "InfiniBrand";
                case 0x07:
                    switch(progif) {
                        case 0x00:
                            return "IPMI SMIC Interface";
                        case 0x01:
                            return "IPMI KCS Interface";
                        case 0x02:
                            return "IPMI Block Transfer Interface";
                        default:
                            return "Unknown IPMI";
                    }
                case 0x08:
                    return "IEC 61491 SERCOS Interface";
                case 0x09:
                    return "CANbus";
                default:
                    return "Unknown Serial Bus Controller";
            }
        case 0xd:
            switch(subclass) {                            
                case 0x00:
                    return "iRDA Controller";
                case 0x01:
                    return "IR Controller";
                case 0x10:
                    return "RF controller";
                case 0x11:
                    return "Bluetooth Controller";
                case 0x12:
                    return "Broadband Controller";
                case 0x20:
                    return "802.11a Ethernet Controller";
                case 0x21:
                    return "802.11b Ethernet Controller";
                case 0x80:
                    return "Other Wireless Controller";
                default:
                    return "Unknown Wireless Controller";
            }
        case 0xe:
            switch(subclass) {                            
                case 0x00:
                    switch(progif) {
                        case 0x00:
                            return "Message FIFO";
                        default:
                            return "I20 Architecture";
                    }
                default:
                    return "Unknown Intelligent I/O";
            }
        case 0xf:
            switch(subclass) {                             
                case 0x01:
                    return "TV Controller";
                case 0x02:
                    return "Audio Controller";
                case 0x03:
                    return "Voice Controller";
                case 0x04:
                    return "Data Controller";
                default:
                    return "Unknown SAT Com Controller";
            }
        case 0x10:
            switch(subclass) {                             
                case 0x00:
                    return "Network/Computing Crypto Controller";
                case 0x10:
                    return "Entertainment Crypto Controller";
                case 0x80:
                    return "Other Crypto Controller";
                default:
                    return "Unknown Crypto Controller";
            }
        case 0x11:
            switch(subclass) {                             
                case 0x00:
                    return "DPIO Modules";
                case 0x01:
                    return "Performance Counters";
                case 0x10:
                    return "CSPT & Frequency Test";
                case 0x20:
                    return "Management Card";
                case 0x80:
                    return "Other DAC/Signal Processing Controller";
                default:
                    return "Unknown DAC/Signal Processing Controller";
            }
    }

    return "<Unknown>";
}

static uint32_t pci_get_header_size(pci_header* h) {
    if(h == 0)
        return 0;

    if((h->base.header_type&0x7F) == 0) {
        return (sizeof(_pci_header_type00) + sizeof(_pci_base_header))/4;
    } else if((h->base.header_type&0x7F) == 1) {
        return (sizeof(_pci_header_type01) + sizeof(_pci_base_header))/4;
    } else {
        return (sizeof(pci_header)-4)/4;
    }
}

static uint16_t pci_read_vendor(uint8_t bus, uint8_t device, uint8_t func) {
    uint32_t addr = PCI_ADDR_DWORD(bus, device, func, 0);
    outd(addr, PCI_CONFIG_ADDRESS);
    return (uint16_t)(ind(PCI_CONFIG_DATA) & 0xFFFF);
}

static void pci_read_header(pci_header* tmp, uint8_t bus, uint8_t device, uint8_t func) {
    uint32_t *header = (void*)tmp, addr = 0;
    for(uint32_t i = 0; i < 4; i++) {
        addr = PCI_ADDR_DWORD(bus, device, func, (i*4));
        outd(addr, PCI_CONFIG_ADDRESS);
        header[i] = ind(PCI_CONFIG_DATA);
    }

    for(uint32_t i = 4; i < pci_get_header_size(tmp); i++) {
        addr = PCI_ADDR_DWORD(bus, device, func, (i*4));
        outd(addr, PCI_CONFIG_ADDRESS);
        header[i] = ind(PCI_CONFIG_DATA);
    }

    tmp->address = PCI_ADDR_DWORD(bus, device, func, 0);
}

static void pci_scan_bus(pci_device_tree* node, uint8_t bus) {
    pci_device_tree *tmp = 0, *tmp2 = 0;

    for(uint8_t dev = 1; dev < 32; dev++) {
        if(pci_read_vendor(bus, dev, 0) != 0xFFFF) {
            tmp2 = 0;

            if(node->children == 0) {
                LINKED_LIST_N(pci_device_tree, tmp);
                tmp->children = 0;
                node->children = tmp;
            } else {
                tmp = node->children;
                LINKED_LIST_LAST(tmp);
                tmp->___next = kalloc(sizeof(pci_device_tree));
                LINKED_LIST_NEXT(tmp);
                tmp->___next = 0;
                tmp->children = 0;
            }

            pci_read_header(&tmp->header, bus, dev, 0);
            printf("%![PCI]%! [0x%hb:%s]%! %s\n", COLOR_WHITE, COLOR_BR_GREEN, tmp->header.base.header_type, pci_get_device_class(&tmp->header), COLOR_GREEN, pci_get_device_type(&tmp->header));

            if((tmp->header.base.class == 0x06) && (tmp->header.base.subclass == 0x04)) {
                pci_scan_bus(tmp, tmp->header.type1.sec_bus_num);
            }

            if(pci_is_multifunc(&tmp->header)) {
                for(uint8_t i = 1; i < 8; i++) {
                    if(pci_read_vendor(bus, dev, i) != 0xFFFF) {
                        if(tmp->children == 0) {
                            LINKED_LIST_N(pci_device_tree, tmp2);
                            tmp2->children = 0;
                            tmp->children = tmp2;
                        } else {
                            tmp2 = tmp->children;
                            LINKED_LIST_LAST(tmp2);
                            tmp2->___next = kalloc(sizeof(pci_device_tree));
                            LINKED_LIST_NEXT(tmp2);
                            tmp2->___next = 0;
                            tmp2->children = 0;
                        }
                        pci_read_header(&tmp2->header, bus, dev, i);
                        printf("%![PCI]%! [0x%hb:%s]%! %s\n", COLOR_WHITE, COLOR_BR_GREEN, tmp2->header.base.header_type, pci_get_device_class(&tmp2->header), COLOR_GREEN, pci_get_device_type(&tmp2->header));
                        if((tmp2->header.base.class == 0x06) && (tmp2->header.base.subclass == 0x04)) {
                            pci_scan_bus(tmp2, tmp2->header.type1.sec_bus_num);
                        }
                    }
                }
            }
        }
    }
}

void pci_scan_devices() {
    pci_device_tree *tmp = 0, *tmp2 = 0;

    if(pci_read_vendor(0, 0, 0) != 0xFFFF) {
        LINKED_LIST_N(pci_device_tree, PCI_TREE);
        PCI_TREE->children = 0;
        pci_read_header(&PCI_TREE->header, 0, 0, 0);

        printf("%![PCI]%! [0x%hb:%s]%! %s\n", COLOR_WHITE, COLOR_BR_GREEN, PCI_TREE->header.base.header_type, pci_get_device_class(&PCI_TREE->header), COLOR_GREEN, pci_get_device_type(&PCI_TREE->header));
        if(pci_is_multifunc(&PCI_TREE->header)) {
            pci_scan_bus(PCI_TREE, 0);
            tmp = PCI_TREE;

            for(uint8_t i = 1; i < 8; i++) {
                if(pci_read_vendor(0, 0, i) == 0xFFFF)
                    continue;
                LINKED_LIST_N(pci_device_tree, tmp2);
                tmp->___next = tmp2;
                LINKED_LIST_NEXT(tmp);
                pci_read_header(&tmp->header, 0, 0, i);
                printf("%![PCI]%! [0x%hb:%s]%! %s\n", COLOR_WHITE, COLOR_BR_GREEN, tmp->header.base.header_type, pci_get_device_class(&tmp->header), COLOR_GREEN, pci_get_device_type(&tmp->header));
                pci_scan_bus(tmp, i);
            }
        } else {
            pci_scan_bus(PCI_TREE, 0);
        }
    } else {
        printf("%![PCI] Something is fucked\n", COLOR_RED);
    }
}

void pci_dump() {
    pci_device_tree* root = PCI_TREE;
    pci_device_tree* node = root->children;
    pci_device_tree* nc = 0;

    while(root != 0) {
        node = root->children;
        printf("%![PCI]%! <ROOT %ib> %s: %s\n", COLOR_WHITE, COLOR_BR_GREEN, PCI_ADDR_BUS(root->header.address), pci_get_device_class(&root->header), pci_get_device_type(&root->header));
        
        while(node != 0) {
            nc = node->children;
            printf("%![PCI]%!   <DEVICE %ib:%ib> %s: %s\n", COLOR_WHITE, COLOR_BR_GREEN, PCI_ADDR_BUS(node->header.address), PCI_ADDR_DEV(node->header.address), pci_get_device_class(&node->header), pci_get_device_type(&node->header));
            
            while(nc != 0) {
                printf("%![PCI]%! \t<DEVICE %ib:%ib:%ib> %s: %s\n", COLOR_WHITE, COLOR_BR_GREEN, PCI_ADDR_BUS(nc->header.address), PCI_ADDR_DEV(nc->header.address), PCI_ADDR_FUN(nc->header.address), pci_get_device_class(&nc->header), pci_get_device_type(&nc->header));
                LINKED_LIST_NEXT(nc);
            }
            LINKED_LIST_NEXT(node);
        }
        LINKED_LIST_NEXT(root);
    } 
}

pci_header* pci_find_class(uint8_t class) {
    pci_device_tree* root = PCI_TREE, *node = 0, *sn = 0;

    while(root != 0) {
        if(root->header.base.class == class) {
            return &root->header;
        }

        node = root->children;
        while(node != 0) {
            if(node->header.base.class == class) {
                return &node->header;
            }

            sn = node->children;
            while(sn != 0) {
                if(sn->header.base.class == class) {
                    return &sn->header;
                }
                LINKED_LIST_NEXT(sn);
            }
            LINKED_LIST_NEXT(node);
        }
        LINKED_LIST_NEXT(root);
    }

    return 0;
}

pci_header* pci_find_subclass(uint8_t class, uint8_t subclass) {
    pci_device_tree* root = PCI_TREE, *node = 0, *sn = 0;

    while(root != 0) {
        if((root->header.base.class == class) && (root->header.base.subclass == subclass)) {
            return &root->header;
        }

        node = root->children;
        while(node != 0) {
            if((node->header.base.class == class) && (node->header.base.subclass == subclass)) {
                return &node->header;
            }

            sn = node->children;
            while(sn != 0) {
                if((sn->header.base.class == class) && (sn->header.base.subclass == subclass)) {
                    return &sn->header;
                }
                LINKED_LIST_NEXT(sn);
            }
            LINKED_LIST_NEXT(node);
        }
        LINKED_LIST_NEXT(root);
    }

    return 0;
}

pci_header* pci_find_next_class(pci_header* last) {
    if(last == 0)
        return 0;

    uint8_t class = last->base.class;
    pci_device_tree* root = PCI_TREE, *node = 0, *sn = 0;

    while(root != 0) {
        if(root->header.base.class == class) {
            if((&root->header) != last)
                return &root->header;
        }

        node = root->children;
        while(node != 0) {
            if(node->header.base.class == class) {
                if((&node->header) != last)
                    return &node->header;
            }

            sn = node->children;
            while(sn != 0) {
                if(sn->header.base.class == class) {
                    if((&sn->header) != last)
                        return &sn->header;
                }
                LINKED_LIST_NEXT(sn);
            }
            LINKED_LIST_NEXT(node);
        }
        LINKED_LIST_NEXT(root);
    }

    return 0;
}

pci_header* pci_find_next_subclass(pci_header* last) {
    if(last == 0)
        return 0;

    uint8_t class = last->base.class;
    uint8_t subclass = last->base.subclass;
    pci_device_tree* root = PCI_TREE, *node = 0, *sn = 0;

    while(root != 0) {
        if((root->header.base.class == class) && (root->header.base.subclass == subclass)) {
            if((&root->header) != last)
                return &root->header;
        }

        node = root->children;
        while(node != 0) {
            if((node->header.base.class == class) && (node->header.base.subclass == subclass)) {
                if((&node->header) != last)
                    return &node->header;
            }

            sn = node->children;
            while(sn != 0) {
                if((sn->header.base.class == class) && (sn->header.base.subclass == subclass)) {
                    if((&sn->header) != last)
                        return &sn->header;
                }
                LINKED_LIST_NEXT(sn);
            }
            LINKED_LIST_NEXT(node);
        }
        LINKED_LIST_NEXT(root);
    }

    return 0;
}
