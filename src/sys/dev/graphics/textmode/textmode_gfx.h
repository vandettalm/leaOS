#ifndef TEXTMODE_GFX_H
#define TEXTMODE_GFX_H

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

#include"../../../core/locks.h"
#include"../../../../util/conversion.h"
#include"../../../mem/mem_util.h"
#include"../../../../util/string.h"

#define COLOR_BLACK     (0x00)
#define COLOR_BLUE      (0x01)
#define COLOR_GREEN     (0x02)
#define COLOR_CYAN      (0x03)
#define COLOR_RED       (0x04)
#define COLOR_VIOLET    (0x05)
#define COLOR_YELLOW    (0x06)
#define COLOR_WHITE     (0x07)

#define COLOR_BR_BLACK  (0x08)
#define COLOR_BR_BLUE   (0x09)
#define COLOR_BR_GREEN  (0x0A)
#define COLOR_BR_CYAN   (0x0B)
#define COLOR_BR_RED    (0x0C)
#define COLOR_BR_VIOLET (0x0D)
#define COLOR_BR_YELLOW (0x0E)
#define COLOR_BR_WHITE  (0x0F)

#define COLOR_DEFAULT   (0x0A)

#define FG_COLOR(c) (c)
#define BG_COLOR(c) ((c)<<4)
#define FB_COLOR(f, b) (FG_COLOR((f)) | BG_COLOR((b)))

uint8_t* get_video_memory();
void set_video_memory(uint8_t* p);

void puts(const char*s, uint16_t x, uint16_t y, uint8_t color);

void dbg_print_memory(uint32_t addr, uint32_t count, uint16_t x, uint16_t y, uint8_t c);

void fill_screen(uint8_t ch, uint8_t c);
void clear_screen();
void scroll_up();

void increment_cursor();
void newline();

void printf(const char *fmt, ...);
void _printf(const char *fmt, ...);


#endif