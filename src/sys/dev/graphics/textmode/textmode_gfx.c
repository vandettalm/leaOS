#include "textmode_gfx.h"

#define POS_OFFSET (((console_y*screen_width) + console_x)*2)

static CREATE_LOCK(_VMEM);
static CREATE_LOCK(_PSTATE);

static uint32_t screen_width = 80;
static uint32_t screen_height = 25;
static uint8_t *video_memory = (void*)0xb8000;
static uint16_t console_x = 0;
static uint16_t console_y = 0;
static uint8_t console_c = 0x0A;

static void _putc(uint8_t c) {
    video_memory[POS_OFFSET] = c;
    video_memory[POS_OFFSET + 1] = console_c;
    increment_cursor();
}

uint8_t *get_video_memory() {
    return video_memory;
}

void set_video_memory(uint8_t* p) {
    WAIT_SET_LOCK(_VMEM);
    video_memory = p;
    UNLOCK(_VMEM);
}

void puts(const char*s, uint16_t x, uint16_t y, uint8_t color) {
    WAIT_SET_LOCK(_VMEM);
    uint8_t* vid_start = video_memory + ((y*screen_width) + x)*2;
    for(char* c = (char*)s; (*c) != 0; c++) {
        vid_start[0] = *c;
        vid_start[1] = color;
        vid_start += 2;
    }
    UNLOCK(_VMEM);
}

void dbg_print_memory(uint32_t addr, uint32_t count, uint16_t x, uint16_t y, uint8_t c) {
    WAIT_SET_LOCK(_VMEM);
    uint8_t *vMem = video_memory + ((y*screen_width) + x)*2;
    uint8_t *rMem = (void*)addr;
    HEX_STR(tmpStr);

    for(uint32_t i = 0; i < count; i++) {
        if (i%16 == 0) {
            vMem = video_memory + (((y+(i/16))*screen_width)+x)*2;
        }
        byte_to_hex(rMem[i], tmpStr);
        vMem[0] = tmpStr[0];
        vMem[2] = tmpStr[1];
        vMem[4] = ' ';
        vMem[1] = c;
        vMem[3] = c;
        vMem[5] = c;
        vMem += 6;
    }
    UNLOCK(_VMEM);
}

void fill_screen(uint8_t ch, uint8_t c) {
    memsetw(video_memory, ((uint16_t)c)<<8 | ch, (screen_width*screen_height));
}

void clear_screen() {
    memset(video_memory, 0, (screen_width*screen_height*2));
}

void scroll_up() {
    memcpy(video_memory, video_memory + (screen_width*2), (screen_width*screen_height*2) - (screen_width*2));
    memset(video_memory + (screen_width*screen_height*2) - (screen_width*2), 0, (screen_width*2));
}

void increment_cursor() {
    console_x++;
    if(console_x >= screen_width) {
        console_x = 0;
        console_y++;

        if(console_y >= screen_height) {
            console_y = screen_height - 1;
            scroll_up();
        }
    }
}

void newline() {
    do {
        increment_cursor();
    } while(console_x != 0);
}

void printf(const char *fmt, ...) {
    WAIT_SET_LOCK(_VMEM);
    char buffer[256];
    char c;
    uint32_t state = 0;
    uint8_t tc = console_c;

    va_list args;
    va_start(args, fmt);

    vsprintf(buffer, fmt, args);

    for(uint32_t i = 0; buffer[i] != 0; i++) {
        c = buffer[i];
        if(state == 0) {
            if(c == '\n') {
                newline();
            } else if(c == '%') {
                state = 1;
            } else {
                _putc(c);
            }
        } else if(state == 1) {
            state = 0;

            if(c == '*') {
                console_c = (uint8_t)buffer[i+1];
                i++;
            } else {
                _putc('%');
                _putc(c);
            }
        }
    }

    va_end(args);
    console_c = tc;
    UNLOCK(_VMEM);
}

/*
void _printf(const char *fmt, ...) {
    WAIT_SET_LOCK(_VMEM);
    va_list args;
    va_start(args, fmt);

    uint64_t tmpQ = 0;
    uint32_t state = 0;
    uint32_t tmpD = 0;
    uint16_t tmpW = 0;
    uint8_t tmpB = 0;

    char c;
    char *tmpStr;

    HEX_STR(tmpHex);
    BIN_STR(tmpBin);

    for(char *str = (char*)fmt; *str != 0; str++) {
        c = *str;
        if(state == 0) {
            if(c == '%') {
                state = 1;
            } else if(c == '\n') {
                newline();
            } 
            else {
                _putc(c);
            }
        } else if(state == 1) {
            if(c == 'c') {
                state = 0;
                tmpB = (uint8_t)va_arg(args, uint32_t);
                _putc(tmpB);
            } else if (c == 'h') {
                if(str[1] == 0) {
                    dword_to_hex((uint32_t)va_arg(args, uint32_t), tmpHex);
                    _puts((uint8_t*)tmpHex);
                } else {
                    state = 2;
                }
            } else if (c == 'i') {
                if(str[1] == 0) {
                    to_decimal((uint64_t)va_arg(args, uint32_t), tmpBin);
                    _puts((uint8_t*)tmpBin);
                } else {
                    state = 3;
                }
            } else if (c == 's') {
                state = 0;
                tmpStr = (char*)va_arg(args, void*);
                _puts((uint8_t*)tmpStr);
            }
        } else if(state == 2) {
            if(c == 'b') {
                byte_to_hex((uint8_t)va_arg(args, uint32_t), tmpHex);
            } else if(c == 'w') {
                word_to_hex((uint16_t)va_arg(args, uint32_t), tmpHex);
            } else if(c == 'd') {
                dword_to_hex((uint32_t)va_arg(args, uint32_t), tmpHex);
            } else if(c == 'q') {
                qword_to_hex(va_arg(args, uint64_t), tmpHex);
            } else {
                dword_to_hex((uint32_t)va_arg(args, uint32_t), tmpHex);
                str--;
            }
            _puts((uint8_t*)tmpHex);
            state = 0;
        } else if(state == 3) {
            if(c == 'b' || c == 'w' || c == 'd') {
                tmpQ = (uint64_t)va_arg(args, uint32_t);
            } else if(c == 'q') {
                tmpQ = va_arg(args, uint64_t);
            } else {
                tmpQ = (uint64_t)va_arg(args, uint32_t);
                str--;
            }
            to_decimal(tmpQ, tmpBin);
            _puts((uint8_t*)tmpBin);
            state = 0;
        }
    }

    va_end(args);
    UNLOCK(_VMEM);
}
*/