#include"ATA.h"
#include"ATAPI.h"

#pragma GCC push_options
#pragma GCC optimize("O0")

static volatile bool GOT_INT = false;
static ata_device *ATA_DEVICE_LIST = 0;

ata_device* ata_get_first_type(uint8_t t) {
    ata_device* ptr = ATA_DEVICE_LIST;
    while((ptr != 0) && ((ptr->type & 0x0F) != t)) {
        LINKED_LIST_NEXT(ptr);
    }
    return ptr;
}

ata_device* ata_get_next_type(ata_device* d) {
    ata_device* ptr = d;
    while((ptr == d) || ((ptr != 0) && (ptr != d) && ((ptr->type & 0x0F) != (d->type&0x0F)))) {
        LINKED_LIST_NEXT(ptr);
    }
    return ptr;
}

ata_device* ata_get_first_hdd() {
    return ata_get_first_type(ATA_TYPE_ATA);
}

ata_device* ata_get_next_hdd(ata_device* d) {
    return ata_get_next_type(d);
}

void ata_add_device(ata_device* d) {
    if(d == 0)
        return;

    if(ATA_DEVICE_LIST == 0) {
        ATA_DEVICE_LIST = d;
    } else {
        ata_device *ptr = ATA_DEVICE_LIST;
        LINKED_LIST_LAST(ptr);
        ptr->___next = d;
    }
}

void ata_irq() {
    GOT_INT = true;
}

void ata_wait_irq() {
    //printf("Waiting on IRQ\n");
    bool t;
    do {
        t = GOT_INT;
    } while(t == false);
    GOT_INT = false;
    //printf("Got Interrupt!\n");
}

uint8_t ata_wait_ready(uint16_t c) {
    //printf("ATA_WAIT_READY\n");
    uint8_t res = 0;
    do {
        res = ata_read_reg(c, 0);
        //printf("0x%hb\n", res);
    } while((((res&ATA_STA_BSY) != 0) && ((res&ATA_STA_DRQ) == 0)) && ((res&ATA_STA_ERR) == 0));
    //printf("READY! 0x%hb\n", res);
    return res;
}

void ata_wait_bsy(uint16_t c, bool t) {
    uint8_t res = 0;
    do {
        res = inb(c);
    } while((((res & ATA_STA_BSY) == 0) == t) && ((res&ATA_STA_ERR) == 0));
}

void ata_wait_drq(uint16_t c, bool t) {
    uint8_t res = 0;
    do {
        res = inb(c);
    } while(((res & ATA_STA_DRQ) == 0) == t);
}

void ata_reset(uint16_t c) {
    out(ATA_COM_RESET, c);
    out(0, c);
}

void ata_select_drive(uint16_t d, uint16_t c, uint8_t dr) {
    ata_wait_bsy(c, false);
    out(dr, d + ATA_REG_DRV_HEAD);
    ata_wait(c);
    ata_wait_bsy(c, false);
}

void ata_send_command(uint16_t d, uint16_t c, uint8_t com) {
    ata_wait_bsy(c, false);
    out(com, d + ATA_REG_COMMAND);
}

void ata_write_reg(uint16_t p, uint8_t r, uint8_t w) {
    out(w, p+r);
}

void ata_write_reg16(uint16_t p, uint8_t r, uint16_t w) {
    outw(w, p+r);
}

uint8_t ata_read_reg(uint16_t p, uint8_t r) {
    return in(p + r);
}

uint16_t ata_read_data16(uint16_t p) {
    return inw(p);
}

bool ata_is_atapi(uint8_t a, uint8_t b) {
    return ((a == 0x14) && (b == 0xEB));
}

bool ata_is_sata(uint8_t a, uint8_t b) {
    return ((a == 0x3C) && (b == 0x3C));
}

void ata_init() {
    uint16_t data = 0, ctrl = 0;
    //uint8_t irq = 0;

    pci_header* tmp = pci_find_subclass(PCI_CLASS_MASS_STORAGE, PCI_MAST_IDE);

    while(tmp != 0) {
        printf("%![ATA]%! Found PCI IDE\n", COLOR_BR_BLUE, COLOR_GREEN);
        data = tmp->type0.bar0;
        ctrl = tmp->type0.bar1;
        
        if((data & 0xFE) == 0) {
            data = 0x01F0;
        }

        if((ctrl & 0xFE) == 0) {
            ctrl = 0x03F6;
        }

        ata_discover(data, ctrl, 0x2E);

        data = tmp->type0.bar2;
        ctrl = tmp->type0.bar3;
        
        if((data & 0xFE) == 0) {
            data = 0x0170;
        }

        if((ctrl & 0xFE) == 0) {
            ctrl = 0x0376;
        }

        ata_discover(data, ctrl, 0x2F);

        tmp = pci_find_next_subclass(tmp);
    }
}

void ata_discover(uint16_t d, uint16_t c, uint8_t itr) {
    uint8_t cyl_low, cyl_high, res;
    ata_device *ptr = 0;

    printf("%![ATA]%! Starting ATA Discovery (0x%hw, 0x%hw)\n", COLOR_BR_BLUE, COLOR_GREEN, d, c);

    register_int_callback(itr, &ata_irq);

    for(uint8_t n = ATA_MASTER; n <= ATA_SLAVE; n += 0x10) {
        ata_reset(c);
        ata_select_drive(d, c, n);
        if(((ata_read_reg(c, 0)) & ATA_STA_ERR) == 1)
            continue;
        cyl_low = ata_read_reg(d, ATA_REG_CYL_LOW);
        cyl_high = ata_read_reg(d, ATA_REG_CYL_HIGH);

        if(ata_is_atapi(cyl_low, cyl_high)) {
            ata_add_device(atapi_discover(d, c, n, itr));
        } else if(ata_is_sata(cyl_low, cyl_high)) {
            printf("%![ATA]%! [0x%hb] Found SATA Device\n", COLOR_BR_BLUE, COLOR_GREEN, n);
        } else {
            ata_write_reg(d, ATA_REG_SEC_CNT, 0);
            ata_write_reg(d, ATA_REG_SEC_NUM, 0);
            ata_write_reg(d, ATA_REG_CYL_LOW, 0);
            ata_write_reg(d, ATA_REG_CYL_HIGH, 0);
            ata_send_command(d, c, ATA_COM_IDENTIFY);

            if(ata_read_reg(c, 0) != 0) {
                ata_wait_bsy(c, false);
                ata_wait_irq();
                res = ata_read_reg(d, ATA_REG_STATUS);
                if((res & ATA_STA_ERR) != 0) {
                    ata_add_device(atapi_discover(d, c, n, itr));
                } else {
                    printf("%![ATA]%! [0x%hb] Found ATA Device (0x%hb)\n", COLOR_BR_BLUE, COLOR_GREEN, n, res);
                    ptr = kalloc(sizeof(ata_device));
                    ptr->___next = 0;
                    ptr->data_port = d;
                    ptr->command_port = c;
                    ptr->type = n;
                    ptr->type |= ATA_TYPE_ATA;
                    ptr->irq = itr;
                    for(uint32_t i = 0; i < 256; i++) {
                        ptr->config_space[i] = ata_read_data16(d);
                    }
                    ata_add_device(ptr);
                }
            } else {
                ata_add_device(atapi_discover(d, c, n, itr));
            }
        }
    }

    deregister_int_callback(itr, &ata_irq);
    GOT_INT = false;
}

void ata_dump() {
    uint32_t total_size = 0;
    uint8_t c;
    ata_device* ptr = ATA_DEVICE_LIST;

    while(ptr != 0) {
        printf("%![ATA]%! Device Type: 0x%hb\n", COLOR_BR_BLUE, COLOR_GREEN, ptr->type);
        printf("%![ATA]%!\tConfig: %nw\n", COLOR_BR_BLUE, COLOR_GREEN, ptr->config_space[0]);
        if((ptr->type & 0x0F) == ATA_TYPE_ATA) {
            printf("%![ATA]%!\tHeads: 0x%hw (%iw)\n", COLOR_BR_BLUE, COLOR_GREEN, ptr->config_space[3], ptr->config_space[3]);
            printf("%![ATA]%!\tBpS: 0x%hw (%iw)\n", COLOR_BR_BLUE, COLOR_GREEN, ptr->config_space[5], ptr->config_space[5]);
            printf("%![ATA]%!\tSpT: 0x%hw (%iw)\n", COLOR_BR_BLUE, COLOR_GREEN, ptr->config_space[6], ptr->config_space[6]);
            total_size = ((uint32_t)(ptr->config_space[3]))*((uint32_t)(ptr->config_space[6]+1))*((uint32_t)(ptr->config_space[5]));
            printf("%![ATA]%!\tTotal Storage: %id Bytes\n", COLOR_BR_BLUE, COLOR_GREEN, total_size);
        } else if((ptr->type & 0x0F) == ATA_TYPE_ATAPI) {
            ata_select_device(ptr);
            ata_reset(ATA_C(ptr));
            ata_select_device(ptr);
            ata_write_reg(ATA_C(ptr), 0x00, 0x00);
            ata_write_reg(ATA_D(ptr), ATAPI_REG_FEATURES, 0);
            ata_write_reg(ATA_D(ptr), ATA_REG_CYL_LOW, 0x08);
            ata_write_reg(ATA_D(ptr), ATA_REG_CYL_HIGH, 0x00);
            register_int_callback(ptr->irq, &ata_irq); GOT_INT = false;
            c = atapi_send_command(ATA_D(ptr), ATA_C(ptr), (void*)ATAPI_READ_CAPACITY);
            
            if((c & ATA_STA_ERR) == 0) {
                uint32_t LBA = 0, BL = 0;
                for(;;) {
                    uint16_t abb[4];
                    for(uint32_t i = 0; i < 4; i++) {
                        abb[i] = inw(ATA_D(ptr));
                        c = (abb[i] & 0xFF);
                        abb[i] = (abb[i] >> 8);
                        abb[i] |= (c << 8);
                        /*
                            FOR FUCKS SAKE
                            WHY DID THEY HAVE TO MAKE ENDIANNESS SO CONFUSING
                        */
                    }
 
                    ata_wait_irq();
                    c = ata_read_reg(ATA_D(ptr), ATA_REG_STATUS);
                    if((c & (ATA_STA_BSY | ATA_STA_DRQ)) == 0) {
                        LBA = (((uint32_t)abb[1]) | (((uint32_t)abb[0]) << 16));
                        BL  = (((uint32_t)abb[3]) | (((uint32_t)abb[2]) << 16));
                        break;
                    }
                    
                }
                printf("%![ATA]%!\tLBA: 0x%hd (%id)\n", COLOR_BR_BLUE, COLOR_GREEN, LBA, LBA);
                printf("%![ATA]%!\tBlock Length: 0x%hd (%id)\n", COLOR_BR_BLUE, COLOR_GREEN, BL, BL);
            }
            deregister_int_callback(ptr->irq, &ata_irq);
        }
        LINKED_LIST_NEXT(ptr);
    }
}

void ata_select_device(ata_device* d) {
    ata_select_drive(ATA_D(d), ATA_C(d), (d->type & 0xF0));
}

/*
void ata_discover(uint16_t d, uint16_t c) {

    register_int_callback(0x2E, &ata_irq);

    ata_device *ptr = 0;
    volatile uint8_t res;
    
    for(uint8_t n = ATA_MASTER; n <= ATA_SLAVE; n += 0x10) {
        do {
            res = inb(d + ATA_REG_STATUS);
        } while((res & 0x80) != 0); //wait till not busy

        outb(ATA_COM_RESET, c);
        outb(0, c);
        outb(n, d + ATA_REG_DRV_HEAD);
        uint8_t tmpa = inb(d + ATA_REG_CYL_LOW);
        uint8_t tmpb = inb(d + ATA_REG_CYL_HIGH);

        if((tmpa == 0x14) && (tmpb == 0xEB)) {
            printf("[ATA] Found ATAPI Device\n");
        } else if((tmpa == 0x3c) && (tmpb == 0x3c)) {
            printf("[ATA] Found SATA Device\n");
        } else {
            
            do {
                res = inb(d + ATA_REG_STATUS);
            } while((res & 0x80) != 0); //wait till not busy

            outb(0, d + ATA_REG_SEC_CNT);
            outb(0, d + ATA_REG_SEC_NUM);
            outb(0, d + ATA_REG_CYL_LOW);
            outb(0, d + ATA_REG_CYL_HIGH);
            outb(ATA_COM_IDENTIFY, d + ATA_REG_COMMAND);
            res = inb(d + ATA_REG_STATUS);

            if(res != 0) {
                printf("[ATA] Found ATA Device (0x%hb)\n", res);
                while((res & 0x80) == 1 && ((res& 0x08) == 0)) {
                    res = inb(d + ATA_REG_STATUS);
                }

                ata_wait_irq();inb(d + ATA_REG_STATUS);

                if(ATA_DEVICE_LIST == 0) {
                    ATA_DEVICE_LIST = kalloc(sizeof(ata_device));
                    ptr = ATA_DEVICE_LIST;
                } else {
                    ptr = _get_last(ATA_DEVICE_LIST);
                    ptr->next_device = kalloc(sizeof(ata_device));
                    ptr = ptr->next_device;
                }
                ptr->next_device = 0;
                ptr->data_port = d;
                ptr->command_port = c;
                ptr->type = n;
                ptr->type |= ATA_TYPE_ATA;
                for(uint32_t i = 0; i < 256; i++) {
                    ptr->config_space[i] = inw(d);
                }
                printf("[ATA]\tConfig: %nw\n", ptr->config_space[0]);
                printf("[ATA]\tPCYL: %iw\n", ptr->config_space[1]);
                printf("[ATA]\tCYL: %iw\n", ptr->config_space[54]);
                printf("[ATA]\tTRK: %iw\n", ptr->config_space[55]);
                printf("[ATA]\tSPT: %iw\n", ptr->config_space[56]);
            }
        }
    }
}
*/

#pragma GCC pop_options