#ifndef _ATAPI_H
#define _ATAPI_H

#include"ATA.h"

#define ATAPI_COM_PACKET 0xA0
#define ATAPI_COM_IDENTIFY 0xA1

#define ATAPI_REG_FEATURES 0x01

ata_device* atapi_discover(uint16_t d, uint16_t c, uint8_t dr, uint8_t irq);
uint8_t atapi_send_command(uint16_t d, uint16_t c, void* com);

extern const uint8_t ATAPI_READ_CAPACITY[12];

#endif