#include"ATAPI.h"

#pragma GCC push_options
#pragma GCC optimize("O0")

const uint8_t ATAPI_READ_CAPACITY[12] = {0x25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static uint16_t read_word_count;

ata_device* atapi_discover(uint16_t d, uint16_t c, uint8_t dr, uint8_t irq) {
    uint8_t res;
    ata_device* tmp = 0;

    ata_reset(c);
    ata_select_drive(d, c, dr);
    ata_write_reg(d, ATA_REG_SEC_CNT, 0);
    ata_write_reg(d, ATA_REG_SEC_NUM, 0);
    ata_write_reg(d, ATA_REG_CYL_LOW, 0);
    ata_write_reg(d, ATA_REG_CYL_HIGH, 0);
    ata_send_command(d, c, ATAPI_COM_IDENTIFY);

    if(ata_read_reg(c, 0) != 0) {
        ata_wait_bsy(c, false);
        ata_wait_irq();
        res = ata_read_reg(d, ATA_REG_STATUS);
        if((res & ATA_STA_ERR) == 0) {
            printf("%![ATA]%! [0x%hb] Found ATAPI Device\n", COLOR_BR_BLUE, COLOR_GREEN, dr);
            tmp = kalloc(sizeof(ata_device));
            tmp->___next = 0;
            tmp->data_port = d;
            tmp->command_port = c;
            tmp->type = dr;
            tmp->type |= ATA_TYPE_ATAPI;
            tmp->irq = irq;
            for(uint32_t i = 0; i < 256; i++) {
                tmp->config_space[i] = ata_read_data16(d);
            }
        }
    }

    return tmp;
}

uint8_t atapi_send_command(uint16_t d, uint16_t c, void* com) {
    ata_send_command(d, c, ATAPI_COM_PACKET);
    ata_wait(c);
    ata_wait_drq(c, true);

    for(uint8_t i = 0; i < 6; i++) {
        //printf("Writing Command: 0x%hw\n", ((uint16_t*)com)[i]);
        //printf("0x%hb - ", ata_read_reg(c, 0));
        //ata_write_reg(d, ATA_REG_DATA, ((uint8_t*)com)[i]);
        ata_write_reg16(d, ATA_REG_DATA, ((uint16_t*)com)[i]);
        //printf("0x%hb\n", ata_read_reg(c, 0));
    }
    ata_wait_irq();
    ata_wait(c);
    return ata_read_reg(d, ATA_REG_STATUS);
}

#pragma GCC pop_options