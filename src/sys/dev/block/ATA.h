#ifndef _ATA_H
#define _ATA_H

#include<stdint.h>
#include"../pci/pci.h"
#include"../../core/idt.h"
#include"../../mem/mem.h"
#include"../../mem/mem_util.h"
#include"../../../util/linked_list.h"

#define ATA_TYPE_ATA    0x0
#define ATA_TYPE_ATAPI  0x1
#define ATA_TYPE_SATA   0x2
#define ATA_TYPE_SATAPI 0x3

#define ATA_MASTER 0xA0
#define ATA_SLAVE 0xB0

#define ATA_PRIMARY_DATA 0x1F0
#define ATA_PRIMARY_COM  0x3F6
#define ATA_SECONDARY_DATA 0x170
#define ATA_SECONDARY_COM 0x376

#define ATA_REG_DATA      0x0
#define ATA_REG_ERROR     0x1
#define ATA_REG_SEC_CNT   0x2
#define ATA_REG_SEC_NUM   0x3
#define ATA_REG_CYL_LOW   0x4
#define ATA_REG_CYL_HIGH  0x5
#define ATA_REG_DRV_HEAD  0x6

#define ATA_REG_STATUS    0x7
#define ATA_REG_COMMAND    0x7

#define ATA_COM_IRQ 0x2
#define ATA_COM_RESET 0x4
#define ATA_COM_IDENTIFY 0xEC

#define ATA_STA_BSY 0x80
#define ATA_STA_ERR 0x1
#define ATA_STA_DRQ 0x8

#define ATA_ERR_ABORT 0x04

#define ATA_C(p) ((p)->command_port)
#define ATA_D(p) ((p)->data_port)

LINKED_LIST_START();
    uint8_t type;
    uint8_t irq;
    uint16_t data_port, command_port;
    uint16_t config_space[256];
LINKED_LIST_END(ata_device);

ata_device* ata_get_first_type(uint8_t t);
ata_device* ata_get_next_type(ata_device*);
ata_device* ata_get_first_hdd();
ata_device* ata_get_next_hdd(ata_device*);

void ata_init();
void ata_discover(uint16_t d, uint16_t c, uint8_t itr);
void ata_dump();

void ata_irq();
void ata_wait_irq();
void ata_wait_bsy(uint16_t c, bool t);
void ata_wait_drq(uint16_t c, bool t);
void ata_reset(uint16_t c);
void ata_select_drive(uint16_t d, uint16_t c, uint8_t dr);
void ata_send_command(uint16_t d, uint16_t c, uint8_t com);
void ata_write_reg(uint16_t p, uint8_t r, uint8_t w);
void ata_write_reg16(uint16_t p, uint8_t r, uint16_t w);
uint8_t ata_read_reg(uint16_t p, uint8_t r);
uint16_t ata_read_data16(uint16_t p) ;
bool ata_is_atapi(uint8_t a, uint8_t b);
bool ata_is_sata(uint8_t a, uint8_t b);

void ata_select_device(ata_device* d);
void ata_add_device(ata_device* d);

uint8_t ata_wait_ready(uint16_t c);

#define ata_wait(c) ata_read_reg(c, 0);ata_read_reg(c, 0);ata_read_reg(c, 0);ata_read_reg(c, 0);ata_read_reg(c, 0)

#endif