#ifndef FADT_H
#define FADT_H

#include <stdint.h>
#include <stdbool.h>

#include"ACPI.h"

typedef struct __attribute__((packed)) {
    acpi_sdt header;

    uint32_t firmware_control;
    uint32_t dsdt_addr;
    uint8_t resv1;

    uint8_t prefered_pmp;
    uint16_t sci_int;
    uint32_t smi_com;
    uint8_t acpi_enable;
    uint8_t acpi_disable;
    uint8_t s4_bios_req;
    uint8_t pstate_control;

    uint32_t pm1_a_event_block;
    uint32_t pm1_b_event_block;
    uint32_t pm1_a_control_block;
    uint32_t pm1_b_control_block;
    uint32_t pm2_control_block;
    uint32_t pm_timer_block;
    uint32_t gpe_0_block;
    uint32_t gpe_1_block;

    uint8_t pm1_event_length;
    uint8_t pm1_control_length;
    uint8_t pm2_control_length;
    uint8_t pm_timer_length;
    uint8_t gpe_0_length;
    uint8_t gpe_1_length;
    uint8_t gpe_1_base;

    uint8_t c_state_control;
    uint16_t worst_latency_c2;
    uint16_t worst_latency_c3;
    uint16_t flush_size;
    uint16_t flush_stride;

    uint8_t duty_offset;
    uint8_t duty_width;
    uint8_t day_alarm;
    uint8_t month_alarm;
    uint8_t century;

    uint16_t boot_arch_flags;
    uint8_t resv2;
    uint32_t flags;

    acpi_gas reset_reg;

    uint8_t reset_val;
    uint8_t resv3[3];

    uint64_t x_firmware_control;
    uint64_t x_dsdt_addr;

    acpi_gas x_pm1_a_event_block;
    acpi_gas x_pm1_b_event_block;
    acpi_gas x_pm1_a_control_block;
    acpi_gas x_pm1_b_control_block;
    acpi_gas x_pm2_control_block;
    acpi_gas x_pm_timer_block;
    acpi_gas x_gpe_0_block;
    acpi_gas x_gpe_1_block;

} acpi_fadt;

#endif