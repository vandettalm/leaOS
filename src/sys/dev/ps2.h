#ifndef PS2_H
#define PS2_H

#define PS2_DATA_PORT 0x60
#define PS2_STATUS_REGISTER 0x64
#define PS2_COMMAND_REGISTER 0x64

#define PS2_COM_ENABLE_PORT1 0xAE
#define PS2_COM_DISABLE_PORT1 0xAD
#define PS2_COM_READ_OUTPUT 0xD0


#endif