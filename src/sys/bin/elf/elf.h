#ifndef _ELF_H
#define _ELF_H

#include<stdint.h>

#include"../../dev/graphics/textmode/textmode_gfx.h"
#include"../../../util/string.h"

typedef struct __attribute__((packed)) {
    uint32_t magic;
    uint8_t bits;
    uint8_t endian;
    uint8_t version;
    uint8_t abi;
    uint8_t abi_version;
    uint8_t pad[7];
    uint16_t type;
    uint16_t arch;
    uint32_t version2;

    uint32_t entry;
    uint32_t prog_header_off;
    uint32_t sec_header_off;
    uint32_t flags;

    uint16_t size;
    uint16_t prog_header_size;
    uint16_t prog_header_num;
    uint16_t sec_header_size;
    uint16_t sec_header_num;
    uint16_t name_sec_index;
} elf_file_header;

typedef struct __attribute__((packed)) {
    uint32_t name_offset;
    uint32_t type;
    uint32_t flags;

    uint32_t addr;
    uint32_t offset;
    uint32_t size;

    uint32_t link;
    uint32_t info;

    uint32_t addr_align;
    uint32_t entry_size;
} elf_section_header;

typedef struct __attribute__((packed)) {
    uint32_t name;
    uint32_t addr;
    uint32_t size;
    uint8_t info;
    uint8_t other;
    uint16_t shndx;
} elf_sym;

#endif