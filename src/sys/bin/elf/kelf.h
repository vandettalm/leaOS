#ifndef _KELF_H
#define _KELF_K

#include "elf.h"

void kernel_elf_init(void *hs, uint32_t c, uint32_t sh);
void kernel_elf_viewsym();
void *kernel_get_next_address();

#endif
