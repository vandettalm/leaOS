#include "kelf.h"

static uint32_t header_start;
static uint32_t header_count;

static elf_section_header *header_array;
static elf_section_header *shstrtab;
static elf_section_header *strtab;
static elf_section_header *symtab;
static elf_section_header *ending;

void kernel_elf_init(void *hs, uint32_t c, uint32_t sh) {
    char *tmp_str;
    
    header_start = (uint32_t)hs;
    header_count = c;
    header_array = hs;
    shstrtab = &(header_array[sh]);
    
    for(uint32_t i = 0; i < header_count; i++) {
        tmp_str = (void*)(shstrtab->addr + header_array[i].name_offset);
        if(strcmp(tmp_str, ".strtab")) {
            strtab = &(header_array[i]);
        } else if(strcmp(tmp_str, ".symtab")) {
            symtab = &(header_array[i]);
        } else if(strcmp(tmp_str, ".end")) {
            ending = &(header_array[i]);
        }
    }
}

void kernel_elf_viewsym() {
    uint32_t count = (symtab->size / symtab->entry_size);
    elf_sym *tmp = (void*)symtab->addr;

    for(uint32_t i = 0; i < count; i++) {
        printf("0x%hd %s ", tmp->addr, (strtab->addr + tmp->name));
        tmp++;

        if((i+1)%2 == 0) {
            printf("\n");
        }
    }
}

void *kernel_get_next_address() {
    return (void*)((ending->addr & 0x00FFFFFF) + 0xC0000000);
}