#ifndef CPU_H
#define CPU_H

#include<stdint.h>
#include<stdbool.h>
#include"../mem/mem_util.h"

#define CPU_EXT_FEATURE_SSE3 (1)
#define CPU_EXT_FEATURE_VMX (1<<5)
#define CPU_EXT_FEATURE_SMX (1<<6)
#define CPU_EXT_FEATURE_SSSE3 (1<<9)
#define CPU_EXT_FEATURE_SSE4_1 (1<<19)
#define CPU_EXT_FEATURE_SSE4_2 (1<<20)
#define CPU_EXT_FEATURE_AES (1<<25)
#define CPU_EXT_FEATURE_AVX (1<<28)

#define CPU_FEATURE_FPU (1)
#define CPU_FEATURE_PAE (1<<6)
#define CPU_FEATURE_APIC (1<<9)
#define CPU_FEATURE_ACPI (1<<22)
#define CPU_FEATURE_MMX (1<<23)
#define CPU_FEATURE_SSE (1<<25)
#define CPU_FEATURE_SSE2 (1<<26)
#define CPU_FEATURE_IA64 (1<<30)
#define CPU_FEATURE_PBE (1<<31)


extern void _getcpuid(uint8_t v, uint32_t *a, uint32_t *b, uint32_t *c, uint32_t *d);

typedef struct __attribute__((packed)) {
    uint32_t max_basic; //0
    char id_str[13];

    uint32_t version_info; //1
    uint32_t brand_info;
    uint32_t extf_info;
    uint32_t f_info;

    uint32_t cache_info1; //2
    uint32_t cache_info2;
    uint32_t cache_info3;
    uint32_t cache_info4;

    uint32_t pro_serial1; //3
    uint32_t pro_serial2;

    uint32_t cache_para1; //4
    uint32_t cache_para2;
    uint32_t cache_para3;
    uint32_t cache_para4;

    uint32_t monitor_para1; //5
    uint32_t monitor_para2;
    uint32_t monitor_para3;
    uint32_t monitor_para4;

    uint32_t thermal_flags1; //6
    uint32_t thermal_sens_ints;
    uint32_t thermal_flags2;

    //7 is wtf
} CPUID_info;

uint32_t cpu_get_features();
uint32_t cpu_get_ext_features();

bool cpu_has_feature(uint32_t f);
bool cpu_has_ext_feature(uint32_t f);

void cpu_info_init();

const char* cpu_id_str();

#endif