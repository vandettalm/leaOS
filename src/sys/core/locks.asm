section .text

global _waitlock:function (_waitlock.end - _waitlock)
global _waitsetlock:function (_waitsetlock.end - _waitsetlock)
global _setlock:function (_setlock.end - _setlock)

_waitlock:
    xor eax, eax
    jmp _wsl
    .end:

_waitsetlock:
    or eax, 0x1
    jmp _wsl
    .end:

_wsl:
    push ebp
    mov ebp, esp
    mov ecx, [ebp+8]
    .lp:
        cmp DWORD [ecx], 0x0
        jnz .lp
        test eax, 0x1
        jnz _setlock.set
        
    leave
    ret
    .end:

_setlock:
    push ebp
    mov ebp, esp
    .set:
        lock or DWORD [ebp+8], 0xFFFFFFFF

    leave
    ret
    .end: