#include"cpu.h"

static CPUID_info cpu_info;

void cpu_info_init() {
    uint32_t a, b, c, d;

    _getcpuid(0x00, &a, &b, &c, &d);
    cpu_info.max_basic = a;
    memcpy(cpu_info.id_str, &b, 4);
    memcpy(cpu_info.id_str+4, &d, 4);
    memcpy(cpu_info.id_str+8, &c, 4);
    cpu_info.id_str[12] = 0;

    for(uint32_t i = 1; i < cpu_info.max_basic || i < 7; i++) {
        if(i == 1) {
            _getcpuid(0x01, &a, &b, &c, &d);
            cpu_info.version_info = a;
            cpu_info.brand_info = b;
            cpu_info.extf_info = c;
            cpu_info.f_info = d;
        } else if(i == 2) {
            _getcpuid(0x02, &a, &b, &c, &d);
            cpu_info.cache_info1 = a;
            cpu_info.cache_info2 = b;
            cpu_info.cache_info3 = c;
            cpu_info.cache_info4 = d;
        } else if(i == 3) {
            _getcpuid(0x03, &a, &b, &c, &d);
            cpu_info.pro_serial1 = c;
            cpu_info.pro_serial2 = d;
        } else if(i == 4) {
            _getcpuid(0x04, &a, &b, &c, &d);
            cpu_info.cache_para1 = a;
            cpu_info.cache_para2 = b;
            cpu_info.cache_para3 = c;
            cpu_info.cache_para4 = d;
        } else if(i == 5) {
            _getcpuid(0x05, &a, &b, &c, &d);
            cpu_info.monitor_para1 = a;
            cpu_info.monitor_para2 = b;
            cpu_info.monitor_para3 = c;
            cpu_info.monitor_para4 = d;
        } else if(i == 6) {
            _getcpuid(0x06, &a, &b, &c, &d);
            cpu_info.thermal_flags1 = a;
            cpu_info.thermal_sens_ints = b;
            cpu_info.thermal_flags2 = c;
        }
    }
}

bool cpu_has_feature(uint32_t f) {
    return ((cpu_info.f_info & f) != 0);
}

bool cpu_has_ext_feature(uint32_t f) {
    return ((cpu_info.extf_info & f) != 0);
}

uint32_t cpu_get_features() {
    return cpu_info.f_info;
}
uint32_t cpu_get_ext_features() {
    return cpu_info.extf_info;
}

const char* cpu_id_str() {
    return (const char*)cpu_info.id_str;
}
