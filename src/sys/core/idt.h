#ifndef IDT_H
#define IDT_H

#include<stdint.h>
#include<stdbool.h>

#include"../mem/mem.h"
#include"../mem/mem_util.h"
#include"../dev/graphics/textmode/textmode_gfx.h"

typedef struct __attribute__((packed)) {
    void (*func)();
    void* next;
} idt_callback;

typedef struct __attribute__((packed)) {
    uint16_t size;
    uint32_t start_addr;
} idt_dsc;

typedef struct __attribute__((packed)) {
    uint16_t offset1;
    uint16_t selector;
    uint8_t nil;
    uint8_t type;
    uint16_t offset2;
} idt_entry;

void* idt_init();
void isr_handler(uint32_t error, uint32_t i_num);

void register_int_callback(uint8_t i, void(*f)());
void deregister_int_callback(uint8_t i, void(*f)());

uint64_t cpu_get_ticks();
void tick_sleep(uint64_t n);

extern volatile uint32_t __CALLBACKS__;

#endif