#ifndef LOCKS_H
#define LOCKS_H

#include <stdint.h>

extern void _setlock(volatile uint32_t* lock);
extern void _waitlock(volatile uint32_t* lock);
extern void _waitsetlock(volatile uint32_t* lock);

#define LOCK(v) _setlock(&v)
#define UNLOCK(v) v = 0
#define WAIT_LOCK(v) _waitlock(&v)
#define WAIT_SET_LOCK(v) _waitsetlock(&v)
#define CREATE_LOCK(v) volatile uint32_t v = 0

#endif