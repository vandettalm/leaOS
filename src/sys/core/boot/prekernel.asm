MBALIGN  equ 1<<0
MEMINFO  equ 1<<1
FLAGS    equ MBALIGN | MEMINFO
MAGIC    equ 0x1BADB002
CHECKSUM equ -(MAGIC + FLAGS)

section .multiboot
align 4
    dd MAGIC
    dd FLAGS
    dd CHECKSUM

global _toBoot:function (_toBoot.end - _toBoot)
global _boot:function (_boot.end - _boot)
global _pg_dir
global _gdt_dsc
global _gdt_size
extern kernel_start

section .tmp2

_toBoot:
    mov [_tmp_a], eax
    mov [_tmp_b], ebx

    mov edi, _pg_dir
    xor ecx, ecx
    xor eax, eax
    mov edx, 0x400000
    
    .loop:
        mov ebx, eax
        mov BYTE [edi], 10000011b  ;set page directory entry flags, 4MiB pages
        shr ebx, 4
        mov BYTE [edi + 1], bl   ; these two lines technically not needed, but work since
        and BYTE [edi + 1], 0xF0 ; they'll always set to 0, making pages aligned
        shr ebx, 12
        mov WORD [edi + 2], bx   ; set last two bytes of ebx to last two bytes of page dir

        inc ecx
        add eax, edx ; eax = the base of the next 4MiB page
        add edi, 4   ; next page dir entry
        cmp ecx, 1024 ; after all 1024 entries, stop
        jne .loop
    
    xor eax, eax
    xor ecx, ecx
    mov edi, _pg_dir + (0x00000300*0x00000004) 
    ; page dir entry whose virtual address is 0xC0000000
    
    ; this loop maps the first 8MiB of memory to kernel space (0xC0000000)
    .loop2:
        mov ebx, eax
        shr ebx, 16
        mov WORD [edi + 2], bx
        inc ecx
        add edi, 4
        add eax, edx
        cmp ecx, 2
        jne .loop2

    ; load page dir table
    mov eax, _pg_dir
    mov cr3, eax

    ; enable paging
    mov eax, cr4
    or eax, 0x00000010
    mov cr4, eax

    ; enables 4 MiB pages
    mov eax, cr0
    or eax, 0x80000000
    mov cr0, eax

    lgdt [_gdt_dsc + 0xC0000000]

    mov eax, 0x10
    mov ds, eax
    mov gs, eax
    mov ss, eax
    mov es, eax
    mov fs, eax

    mov eax, [_tmp_a]
    mov ebx, [_tmp_b]

    jmp 0x08:_boot

    .hang:
        hlt
        jmp .hang
    .end:

align 4096
_pg_dir:
    times 1024 dd 0x00000002

    _tmp_a dd 0x00000000
    _tmp_b dd 0x00000000

align 4096
_gdt_dsc:
    _gdt_size dw (_gdt.end - _gdt_dsc) - 1
    dd (_gdt_dsc + 0xC0000000)
    dw 0000
_gdt:
    dd 0x0000FFFF
    dd 0x00FC9A00

    dd 0x0000FFFF
    dd 0x00FC9200
    .end:


section .bss
align 4
stack_bottom:
    resb 16384
stack_top:

section .text

_boot:
    mov esp, stack_top
    push ebx
    push eax

    call kernel_start

    .hang: 
        hlt
        jmp .hang
    .end:

section .end_of_kernel
kernel_end:
    dd 0x00000000