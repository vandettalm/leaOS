#include"multiboot.h"

static multiboot_info mb_info;

void multiboot_init(void* mb_addr) {
    memcpy(&mb_info, mb_addr, sizeof(multiboot_info));
    kernel_elf_init((void*)(mb_info.sym_3), mb_info.sym_1, mb_info.sym_4);
    printf("%![MULTIBOOT]%! Flags: %nd (0x%hd)\n", 0x0D, 0x0B, mb_info.flags, mb_info.flags);
    printf("%![MULTIBOOT]%! Boot Device: 0x%hd\n", 0x0D, 0x0B, mb_info.boot_device);
}

void multiboot_dbg_print() {
    uint32_t* tmp = (uint32_t*)&mb_info;

    printf("%![MULTIBOOT]%! DEBUG (size: %id bytes)\n", 0x0D, 0x0B, sizeof(multiboot_info));
    for(uint32_t i = 0; i < (sizeof(multiboot_info)/4); i++) {
        printf("\t* %!DWORD[%id(%id)]%! 0x%hd (%nd)\n", 0x0C, i, i*4, 0x0B, tmp[i], tmp[i]);
    }
}

uint64_t multiboot_get_memory_map() {
    return (uint64_t)(mb_info.map_addr) | ((uint64_t)(mb_info.map_len) << 32);
}