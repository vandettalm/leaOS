#ifndef MULTIBOOT_H
#define MULTIBOOT_H

#include<stdint.h>

#include"../../bin/elf/kelf.h"
#include"../../mem/mem_util.h"
#include"../../dev/graphics/textmode/textmode_gfx.h"

typedef struct __attribute__((packed)) {
    uint32_t flags;

    uint32_t mem_lower, mem_upper;
    uint32_t boot_device;
    uint32_t cmd;

    uint32_t mods_count;
    uint32_t mods_addr;

    uint32_t sym_1, sym_2, sym_3, sym_4;

    uint32_t map_len, map_addr;
    uint32_t drive_len, drive_addr;

    uint32_t config;

    uint32_t name;

    uint32_t apm_table;

    uint32_t vbe_ctrl_info;
    uint32_t vbe_mode_info;
    uint16_t vbe_mode;
    uint16_t vbe_int_seg;
    uint16_t vbe_int_off;
    uint16_t vbe_int_len;

} multiboot_info;

void multiboot_init(void* mb_addr);
void multiboot_dbg_print();

uint64_t multiboot_get_memory_map();
void* multiboot_get_next_free();

#endif