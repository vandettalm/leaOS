section .text

extern isr_handler
global _isr:function   (_isr.end   - _isr)
global _isr_e:function (_isr_e.end - _isr_e)
global _set_idt:function (_set_idt.end - _set_idt)
global _isr_s
global _isr_e_s

_isr:
    pushad
    push 0x00000000
    push 0x1F1F1F1F
    mov eax, isr_handler
    call eax
    add esp, 8
    popad
    iret
    .end:

_isr_e:
    pop DWORD [__er_st]
    pushad
    push DWORD [__er_st]
    push 0x1F1F1F1F
    mov eax, isr_handler
    call eax
    add esp, 8
    popad
    iret
    .end:

_set_idt:
    push ebp
    mov ebp, esp

    cli
    mov eax, [ebp+8]
    lidt [eax]
    sti

    leave
    ret
    .end:

__er_st:
    dd 0

section .rodata

_isr_s:
    dd (_isr.end   - _isr)
_isr_e_s:
    dd (_isr_e.end - _isr_e)