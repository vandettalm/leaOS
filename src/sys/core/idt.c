#include"idt.h"

#pragma GCC push_options
#pragma GCC optimize("O0")

#define idt_size 0x100

static volatile uint64_t cpu_ticks = 0;
//static const uint32_t idt_size = 0x100;
static void *idt_f_start;
static idt_dsc idt_d;
static idt_entry *idt_ptr;
static volatile idt_callback idt_callbacks[idt_size];
CREATE_LOCK(__CALLBACKS__);

extern void _isr();
extern void _isr_e();
extern void _set_idt(void* addr);
extern uint32_t _isr_s;
extern uint32_t _isr_e_s;

static volatile idt_callback* _get_last(volatile idt_callback* p) {
    if(p->next == 0)
        return p;
    return _get_last(p->next);
}

static volatile idt_callback* _get_free(volatile idt_callback* p) {
    if(p->func == 0)
        return p;
    if(p->next != 0)
        return _get_free(p->next);
    return 0;
}

static inline volatile idt_callback* _get_next(volatile idt_callback* p) {
    return p->next;
}

static void _call_each(volatile idt_callback* f) {
    if(f->func != 0)
        (*(f->func))();
    if(f->next != 0)
        _call_each(f->next);
}

static inline bool _has_code(uint32_t i) {
    return (i == 0x8) || (i >= 0xA && i <= 0xE) || (i == 0x11) || (i == 0x1E);
}

static void pic_init() {
    /*
    uint8_t m1, m2;
    m1 = in(0x21);
    m2 = in(0xA1);
    
    //printf("Master Mask: %nb\n", m1);
    //printf("Slave Mask: %nb\n", m2);
    */
    
    out(0x11, 0x20);
    out(0x11, 0xA0);
    out(0x20, 0x21);
    out(0x28, 0xA1);
    out(4, 0x21);
    out(2, 0xA1);
    out(1, 0x21);
    out(1, 0xA1);

    out(0x00, 0x21); //mask 0x00 = enable all IRQs
    out(0x00, 0xA1);
    //out(m1, 0x21);
    //out(m2, 0xA1);
}

void* idt_init() {
    uint32_t total_size = ((_isr_s > _isr_e_s)?_isr_s:_isr_e_s);
    pic_init();

    void *start_addr = kalloc(((idt_size * total_size) + (sizeof(idt_entry)*idt_size)));

    idt_d.size = (sizeof(idt_entry)*idt_size) - 1;
    idt_d.start_addr = ((uint32_t)start_addr);
    idt_ptr = start_addr;
    idt_f_start = start_addr + (sizeof(idt_entry)*idt_size);

    memset(idt_f_start, 0, (total_size*idt_size));

    //printf("IDT Start: 0x%hd\n", idt_f_start);
    //printf("Size: 0x%hd 0x%hd\n", _isr_s, _isr_e_s);

    void* tmp = idt_f_start;
    for(uint32_t i = 0; i < idt_size; i++) {
        idt_callbacks[i].func = 0;
        idt_callbacks[i].next = 0;
        idt_ptr[i].offset1 = (uint16_t)((uint32_t)tmp & 0xFFFF);
        idt_ptr[i].offset2 = (uint16_t)((uint32_t)tmp >> 16) & 0xFFFF;
        idt_ptr[i].nil = 0x00;
        idt_ptr[i].selector = 0x08;//(uint16_t) i;
        idt_ptr[i].type = 0b10001110;

        if(_has_code(i)) {
            memcpy(tmp, &_isr_e, _isr_e_s);
            ((uint32_t*)(tmp+14))[0] = i;
        } else {
            memcpy(tmp, &_isr, _isr_s);
            ((uint32_t*)(tmp+4))[0] = i;
        }

        tmp += total_size;
    }

    _set_idt(&idt_d);
    return idt_f_start;
}

void isr_handler(uint32_t i_num, uint32_t error) {
    if(i_num == 0x20) {
        cpu_ticks++;
    } else if(i_num != 0x2E && i_num != 0x2F) {
        printf("%!INTERRUPT: [0x%hd, 0x%hd]\n", COLOR_BR_RED, i_num, error);
    }

    if(__CALLBACKS__ == 0) {
        _call_each(&(idt_callbacks[i_num]));
    }

    if(i_num >= 20 && i_num <= 0x2F) {
        if(i_num >= 0x28) {
            out(0x20, 0xA0);
        }
        out(0x20, 0x20);
    }
}

uint64_t cpu_get_ticks() {
    return cpu_ticks;
}

void tick_sleep(uint64_t n) {
    uint64_t start = cpu_ticks;
    for(;;) {
        if((cpu_ticks - start) >= n) {
            return;
        }
    }
}

void register_int_callback(uint8_t i, void(*f)()) {
    WAIT_SET_LOCK(__CALLBACKS__);
    
    /* Only needed if we need more than 0x100 interrupts, would have to change i to uint16_t or uint32_t
    if(i >= idt_size)
        return;
    */

    volatile idt_callback* p = _get_free(&(idt_callbacks[i]));
    if(p == 0) {
        p = _get_last(&(idt_callbacks[i]));
        p->next = kalloc(sizeof(idt_callback));
        p = p->next;
        p->next = 0;
    }
    p->func = f;

    UNLOCK(__CALLBACKS__);
}

void deregister_int_callback(uint8_t i, void(*f)()) {
    WAIT_SET_LOCK(__CALLBACKS__);

    /* Only needed if we need more than 0x100 interrupts, would have to change i to uint16_t or uint32_t
    if(i >= idt_size)
        return;
    */

    volatile idt_callback* p = &(idt_callbacks[i]);
    do {
        if(p->func == f) {
            p->func = 0;
            break;
        }
        p = _get_next(p);
    } while(p != 0);
    
    UNLOCK(__CALLBACKS__);
}

#pragma GCC pop_options