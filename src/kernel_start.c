#include <stdint.h>
#include <stdbool.h>

#include "sys/dev/graphics/textmode/textmode_gfx.h"
#include "util/conversion.h"
#include "util/disasm.h"
#include "util/string.h"
#include "sys/core/cpu.h"
#include "sys/core/boot/multiboot.h"
#include "sys/core/idt.h"
#include "sys/dev/block/drive.h"
#include "sys/dev/block/ATA.h"
#include "sys/dev/ACPI.h"
#include "sys/mem/mem_section.h"
#include "sys/mem/vmm.h"
#include "sys/mem/mem.h"

#include "sys/dev/pci/pci.h"

void init(void *mbi) {
    printf("%![KERNEL]%! Starting initialization\n", FG_COLOR(COLOR_VIOLET), FG_COLOR(COLOR_BR_GREEN));
    save_bda();
    cpu_info_init();
    multiboot_init(mbi);
    //printf("%![KERNEL]%! Next Free Address: 0x%hd\n", FG_COLOR(COLOR_VIOLET), FG_COLOR(COLOR_BR_GREEN), kernel_get_next_address());
    mem_init(kernel_get_next_address(), multiboot_get_memory_map());
    //printf("%![KERNEL]%! Next Free Address: 0x%hd\n", FG_COLOR(COLOR_VIOLET), FG_COLOR(COLOR_BR_GREEN), mem_last_init());
    vmm_init();
    idt_init();
    pci_scan_devices();
    ata_init();
    //ata_discover(ATA_PRIMARY_DATA, ATA_PRIMARY_COM, 0x2E);
    //ata_discover(ATA_SECONDARY_DATA, ATA_SECONDARY_COM, 0x2F);
}

void kernel_start(uint32_t magic, void* mbi) {
    if(magic != 0x2BADB002) {
        printf("Boot Fail\n");
        return;
    }
    printf("%![leaOS 0.07]\n", 0x0D);
    init(mbi);
    
/*
    printf("CPU: %s\n", cpu_id_str());
    ata_dump();
    printf("--Fin--\n");

    ata_device* first_hdd = ata_get_first_hdd();
    printf("First HDD (0x%hd): 0x%hw 0x%hw 0x%hw\n", first_hdd, first_hdd->config_space[3], first_hdd->config_space[5], first_hdd->config_space[6]);
    printf("Single DMA Flags: 0x%hw (%nw)\n", first_hdd->config_space[62], first_hdd->config_space[62]);
    printf("Multi DMA Flags: 0x%hw (%nw)\n", first_hdd->config_space[63], first_hdd->config_space[63]);
    printf("DMA Flags: %hw (%nw)\n", first_hdd->config_space[49], first_hdd->config_space[49]);
*/
    //pci_dump();
    printf("====Done====\n");
    while(true) {}
    
    return;
}